/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>

#include <libgnome/libgnome.h>
#include "e-folder-groupwise.h"
#include "groupwise-connection.h"
#include "groupwise-storage.h"

struct _GroupwiseStoragePrivate {
	GroupwiseAccount *account;

	guint idle_id;
};

enum props {
	PROP_0,
	PROP_ACCOUNT,
};

static GObjectClass *parent_class;

static void
groupwise_storage_init (GroupwiseStorage *gw)
{
	GroupwiseStoragePrivate *priv;

	priv = g_new0 (GroupwiseStoragePrivate, 1);
	gw->priv = priv;
}

static void
groupwise_storage_set_property (GObject *object, guint property_id, 
				   const GValue *value, GParamSpec *pspec)
{
	GroupwiseStorage *gw;
	GroupwiseStoragePrivate *priv;
	
	gw = GROUPWISE_STORAGE (object);
	priv = gw->priv;
	
	switch (property_id) {
	case PROP_ACCOUNT:
		priv->account = GROUPWISE_ACCOUNT (g_value_dup_object (value));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
groupwise_storage_get_property (GObject *object, guint property_id, 
				   GValue *value, GParamSpec *pspec)
{
	GroupwiseStorage *gw;
	GroupwiseStoragePrivate *priv;
	
	gw = GROUPWISE_STORAGE (object);
	priv = gw->priv;

	switch (property_id) {
	case PROP_ACCOUNT:
		g_value_set_object (value, priv->account);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
groupwise_storage_finalize (GObject *object) 
{
	GroupwiseStorage *gw;
	GroupwiseStoragePrivate *priv;
	
	gw = GROUPWISE_STORAGE (object);
	priv = gw->priv;

	g_object_unref (priv->account);
	
	g_free (priv);
}

static void
groupwise_storage_class_init (GroupwiseStorageClass *klass)
{
	GObjectClass *object_class;
	GParamSpec *param;
	
	parent_class = (GObjectClass *) g_type_class_peek_parent (klass);

	object_class = (GObjectClass *) klass;

	object_class->get_property = groupwise_storage_get_property;
	object_class->set_property = groupwise_storage_set_property;
	object_class->finalize = groupwise_storage_finalize;

	param = g_param_spec_object ("account", NULL, NULL, GROUPWISE_TYPE_ACCOUNT,
				     G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
	g_object_class_install_property (object_class, PROP_ACCOUNT, param);
}

GType
groupwise_storage_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static GTypeInfo info = {
                        sizeof (GroupwiseStorageClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) groupwise_storage_class_init,
                        NULL, NULL,
                        sizeof (GroupwiseStorage),
                        0,
                        (GInstanceInitFunc) groupwise_storage_init,
                };
		type = g_type_register_static (EVOLUTION_TYPE_STORAGE, "GroupwiseStorage", &info, 0);
	}

	return type;
}

static gboolean
idle_fill_storage (gpointer user_data)
{
	GroupwiseStorage *gstorage = user_data;
	GroupwiseStoragePrivate *priv;
	EvolutionStorage *storage = user_data;
	GPtrArray *folders;
	int i;
	
	priv = gstorage->priv;
	
	priv->idle_id = 0;

	if (!groupwise_account_connect (priv->account))
		return FALSE;
	
	folders = groupwise_account_get_folders (priv->account);
	
	for (i = 0; i < folders->len; i++) {
		EFolder *folder = folders->pdata[i];
		const char *path;

		path = e_folder_groupwise_get_path (E_FOLDER_GROUPWISE (folder));
		
		g_message ("Adding folder %s", path);
		
		evolution_storage_new_folder (storage, path,
					      e_folder_get_name (folder),
					      e_folder_get_type_string (folder),
					      e_folder_get_physical_uri (folder),
					      e_folder_get_description (folder),
					      e_folder_get_custom_icon_name (folder),
					      e_folder_get_unread_count (folder),
					      e_folder_get_can_sync_offline (folder),
					      e_folder_get_sorting_priority (folder));
	
		if (e_folder_groupwise_get_has_subfolders (E_FOLDER_GROUPWISE (folder)))
			evolution_storage_has_subfolders (storage, path, _("Searching..."));
	}

	g_ptr_array_free (folders, FALSE);
	
	return FALSE;
}

GroupwiseStorage *
groupwise_storage_new (GroupwiseAccount *account)
{
	GroupwiseStorage *gstorage;
	
	gstorage = g_object_new (GROUPWISE_TYPE_STORAGE, "account", account, NULL);

	evolution_storage_construct (EVOLUTION_STORAGE (gstorage), groupwise_account_get_name (account), TRUE);

	gstorage->priv->idle_id = g_idle_add (idle_fill_storage, gstorage);

	return gstorage;
}
