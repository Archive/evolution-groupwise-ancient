/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GROUPWISE_ACCOUNT_H
#define GROUPWISE_ACCOUNT_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS



#define GROUPWISE_TYPE_ACCOUNT            (groupwise_account_get_type ())
#define GROUPWISE_ACCOUNT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GROUPWISE_TYPE_ACCOUNT, GroupwiseAccount))
#define GROUPWISE_ACCOUNT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GROUPWISE_TYPE_ACCOUNT,		\
				     GroupwiseAccountClass))
#define GROUPWISE_IS_ACCOUNT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GROUPWISE_TYPE_ACCOUNT))
#define GROUPWISE_IS_ACCOUNT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GROUPWISE_TYPE_ACCOUNT))

/* Open status values */
typedef struct _GroupwiseAccount GroupwiseAccount;
typedef struct _GroupwiseAccountClass GroupwiseAccountClass;
typedef struct _GroupwiseAccountPrivate GroupwiseAccountPrivate;

struct _GroupwiseAccount {
	GObject parent;

	GroupwiseAccountPrivate *priv;
};

struct _GroupwiseAccountClass {
	GObjectClass parent_class;
};

GType             groupwise_account_get_type          (void);
GroupwiseAccount *groupwise_account_new               (const char       *name,
						       const char       *uri,
						       const char       *user,
						       const char       *password);
GroupwiseAccount *groupwise_account_new_from_existing (GroupwiseAccount *existing_account);
gboolean          groupwise_account_connect           (GroupwiseAccount *account);
GPtrArray *       groupwise_account_get_folders       (GroupwiseAccount *acct);
const char *      groupwise_account_get_name          (GroupwiseAccount *account);
void              groupwise_account_set_name          (GroupwiseAccount *account,
						       const char       *name);
const char *      groupwise_account_get_uri           (GroupwiseAccount *account);
void              groupwise_account_set_uri           (GroupwiseAccount *account,
						       const char       *uri);
const char *      groupwise_account_get_user          (GroupwiseAccount *account);
void              groupwise_account_set_user          (GroupwiseAccount *account,
						       const char       *user);
const char *      groupwise_account_get_password      (GroupwiseAccount *account);
void              groupwise_account_set_password      (GroupwiseAccount *account,
						       const char       *password);
gboolean          groupwise_account_is_complete       (GroupwiseAccount *account);


G_END_DECLS

#endif
