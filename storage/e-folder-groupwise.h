/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef E_FOLDER_GROUPWISE_H
#define E_FOLDER_GROUPWISE_H

#include <glib.h>
#include <glib-object.h>
#include <shell/e-folder.h>

G_BEGIN_DECLS



#define E_TYPE_FOLDER_GROUPWISE            (e_folder_groupwise_get_type ())
#define E_FOLDER_GROUPWISE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), E_TYPE_FOLDER_GROUPWISE, EFolderGroupwise))
#define E_FOLDER_GROUPWISE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), E_TYPE_FOLDER_GROUPWISE,		\
				     EFolderGroupwiseClass))
#define E_IS_FOLDER_GROUPWISE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), E_TYPE_FOLDER_GROUPWISE))
#define E_IS_FOLDER_GROUPWISE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), E_TYPE_FOLDER_GROUPWISE))

/* Open status values */
typedef struct _EFolderGroupwise EFolderGroupwise;
typedef struct _EFolderGroupwiseClass EFolderGroupwiseClass;
typedef struct _EFolderGroupwisePrivate EFolderGroupwisePrivate;

struct _EFolderGroupwise {
	EFolder parent;

	EFolderGroupwisePrivate *priv;
};

struct _EFolderGroupwiseClass {
	EFolderClass parent_class;
};

GType             e_folder_groupwise_get_type           (void);
EFolderGroupwise *e_folder_groupwise_new                (const char       *name,
							 const char       *type,
							 const char       *description);
gboolean          e_folder_groupwise_get_has_subfolders (EFolderGroupwise *folder);
void              e_folder_groupwise_set_has_subfolders (EFolderGroupwise *folder,
							 gboolean          has_subfolders);

const char *e_folder_groupwise_get_path (EFolderGroupwise *folder);
void              e_folder_groupwise_set_path (EFolderGroupwise *folder,
					       const char *path);

G_END_DECLS

#endif
