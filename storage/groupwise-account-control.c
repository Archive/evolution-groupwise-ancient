/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>

#include <libgnome/libgnome.h>
#include <gtk/gtk.h>
#include <glade/glade-xml.h>

#include "groupwise-account-control.h"

struct _GroupwiseAccountControlPrivate {
	GroupwiseAccount *account;

	GladeXML *xml;
};

enum props {
	PROP_0,
	PROP_ACCOUNT
};

static GObjectClass *parent_class;

static void
groupwise_account_control_init (GroupwiseAccountControl *gw)
{
	GroupwiseAccountControlPrivate *priv;

	priv = g_new0 (GroupwiseAccountControlPrivate, 1);
	gw->priv = priv;
}

static void
groupwise_account_control_set_property (GObject *object, guint property_id, 
					const GValue *value, GParamSpec *pspec)
{
	GroupwiseAccountControl *control;
	GroupwiseAccountControlPrivate *priv;
	
	control = GROUPWISE_ACCOUNT_CONTROL (object);
	priv = control->priv;
	
	switch (property_id) {
	case PROP_ACCOUNT:
		priv->account = GROUPWISE_ACCOUNT (g_value_dup_object (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
groupwise_account_control_get_property (GObject *object, guint property_id, 
					GValue *value, GParamSpec *pspec)
{
	GroupwiseAccountControl *control;
	GroupwiseAccountControlPrivate *priv;
	
	control = GROUPWISE_ACCOUNT_CONTROL (object);
	priv = control->priv;

	switch (property_id) {
	case PROP_ACCOUNT:
		g_value_set_object (value, priv->account);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
groupwise_account_control_finalize (GObject *object) 
{
	GroupwiseAccountControl *gw;
	GroupwiseAccountControlPrivate *priv;
	
	gw = GROUPWISE_ACCOUNT_CONTROL (object);
	priv = gw->priv;

	if (priv->account)
		g_object_unref (priv->account);

	if (priv->xml)
		g_object_unref (priv->xml);
	
	g_free (priv);
}

static void
groupwise_account_control_class_init (GroupwiseAccountControlClass *klass)
{
	GObjectClass *object_class;
	GParamSpec *param;
	
	parent_class = (GObjectClass *) g_type_class_peek_parent (klass);

	object_class = (GObjectClass *) klass;

	object_class->get_property = groupwise_account_control_get_property;
	object_class->set_property = groupwise_account_control_set_property;
	object_class->finalize = groupwise_account_control_finalize;

	param = g_param_spec_object ("account", NULL, NULL, GROUPWISE_TYPE_ACCOUNT,
				     G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
	g_object_class_install_property (object_class, PROP_ACCOUNT, param);
}

GType
groupwise_account_control_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static GTypeInfo info = {
                        sizeof (GroupwiseAccountControlClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) groupwise_account_control_class_init,
                        NULL, NULL,
                        sizeof (GroupwiseAccountControl),
                        0,
                        (GInstanceInitFunc) groupwise_account_control_init,
                };
		type = g_type_register_static (EVOLUTION_TYPE_CONFIG_CONTROL, "GroupwiseAccountControl", &info, 0);
	}

	return type;
}

static void
account_name_changed_cb (GtkWidget *widget, gpointer data)
{
	GroupwiseAccountControl *control;
	GroupwiseAccountControlPrivate *priv;
	const char *text;
	
	control = GROUPWISE_ACCOUNT_CONTROL (data);
	priv = control->priv;

	text = gtk_entry_get_text (GTK_ENTRY (widget));
	groupwise_account_set_name (priv->account, text);

	if (groupwise_account_is_complete (priv->account))
		evolution_config_control_changed (EVOLUTION_CONFIG_CONTROL (control));
}

static void
account_uri_changed_cb (GtkWidget *widget, gpointer data)
{
	GroupwiseAccountControl *control;
	GroupwiseAccountControlPrivate *priv;
	const char *text;
	
	control = GROUPWISE_ACCOUNT_CONTROL (data);
	priv = control->priv;

	text = gtk_entry_get_text (GTK_ENTRY (widget));
	groupwise_account_set_uri (priv->account, text);

	if (groupwise_account_is_complete (priv->account))
		evolution_config_control_changed (EVOLUTION_CONFIG_CONTROL (control));
}

static void
account_user_changed_cb (GtkWidget *widget, gpointer data)
{
	GroupwiseAccountControl *control;
	GroupwiseAccountControlPrivate *priv;
	const char *text;
	
	control = GROUPWISE_ACCOUNT_CONTROL (data);
	priv = control->priv;

	text = gtk_entry_get_text (GTK_ENTRY (widget));
	groupwise_account_set_user (priv->account, text);

	if (groupwise_account_is_complete (priv->account))
		evolution_config_control_changed (EVOLUTION_CONFIG_CONTROL (control));
}

static void
account_password_changed_cb (GtkWidget *widget, gpointer data)
{
	GroupwiseAccountControl *control;
	GroupwiseAccountControlPrivate *priv;
	const char *text;
	
	control = GROUPWISE_ACCOUNT_CONTROL (data);
	priv = control->priv;

	text = gtk_entry_get_text (GTK_ENTRY (widget));
	groupwise_account_set_password (priv->account, text);

	if (groupwise_account_is_complete (priv->account))
		evolution_config_control_changed (EVOLUTION_CONFIG_CONTROL (control));
}

static void
fill_widgets (GroupwiseAccountControl *control)
{
	GroupwiseAccountControlPrivate *priv;
	GtkWidget *entry;
	
	priv = control->priv;

	entry = glade_xml_get_widget (priv->xml, "account_name");
	gtk_entry_set_text (GTK_ENTRY (entry), groupwise_account_get_name (priv->account));
	g_signal_connect (G_OBJECT (entry), "changed", G_CALLBACK (account_name_changed_cb), control);

	entry = glade_xml_get_widget (priv->xml, "account_uri");
	gtk_entry_set_text (GTK_ENTRY (entry), groupwise_account_get_uri (priv->account));
	g_signal_connect (G_OBJECT (entry), "changed", G_CALLBACK (account_uri_changed_cb), control);

	entry = glade_xml_get_widget (priv->xml, "account_user");
	gtk_entry_set_text (GTK_ENTRY (entry), groupwise_account_get_user (priv->account));
	g_signal_connect (G_OBJECT (entry), "changed", G_CALLBACK (account_user_changed_cb), control);

	entry = glade_xml_get_widget (priv->xml, "account_password");
	gtk_entry_set_text (GTK_ENTRY (entry), groupwise_account_get_password (priv->account));
	g_signal_connect (G_OBJECT (entry), "changed", G_CALLBACK (account_password_changed_cb), control);
}

static BonoboObject *
dummy_control (const char *message)
{
	GtkWidget *label;

	label = gtk_label_new (message);
	gtk_widget_show (label);

	return BONOBO_OBJECT (evolution_config_control_new (label));
}

BonoboObject *
groupwise_account_control_new (GroupwiseAccount *account)
{
	GroupwiseAccountControl *control;
	GroupwiseAccountControlPrivate *priv;
	GtkWidget *table;
	
	control = g_object_new (GROUPWISE_TYPE_ACCOUNT_CONTROL, "account", account, NULL);
	priv = control->priv;
	
	priv->xml = glade_xml_new (GLADEDIR "/account-config.glade", "account_table", NULL);
	if (!priv->xml) {
		bonobo_object_unref (BONOBO_OBJECT (control));
		return dummy_control (_("Unable to load delegate configuration UI."));
	}

	/* Put the (already parentless) glade widgets into the control */
	table = glade_xml_get_widget (priv->xml, "account_table");
	evolution_config_control_construct (EVOLUTION_CONFIG_CONTROL (control), table);

	fill_widgets (control);
	
	return BONOBO_OBJECT (control);
}
