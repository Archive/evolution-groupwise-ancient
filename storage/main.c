/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <bonobo-activation/bonobo-activation.h>
#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-generic-factory.h>
#include <bonobo/bonobo-moniker-util.h>
#include <bonobo/bonobo-exception.h>
#include <libgnomeui/gnome-ui-init.h>
#include <libgnomevfs/gnome-vfs-init.h>
#include <gconf/gconf-client.h>
#include <libsoup/soup-misc.h>
#include <gtk/gtkmessagedialog.h>
#include <glade/glade.h>
#include <gconf/gconf-client.h>

#include <e-util/e-passwords.h>
#include <pas/pas-book-factory.h>
#include <pcs/cal-factory.h>
#include <shell/evolution-shell-component.h>

#include "cal-backend-groupwise.h"
#include "groupwise-account.h"
#include "groupwise-account-control.h"
#include "groupwise-storage.h"

#define GROUPWISE_STORAGE_COMPONENT_ID	"OAFIID:GNOME_Evolution_GroupwiseStorage_ShellComponent"
#define GROUPWISE_CALENDAR_FACTORY_ID	"OAFIID:GNOME_Evolution_GroupwiseStorage_CalendarFactory"
#define GROUPWISE_CONFIG_FACTORY_ID	"OAFIID:GNOME_Evolution_GroupwiseStorage_ConfigFactory"
#define GROUPWISE_ACCOUNT_CONTROL_ID	"OAFIID:GNOME_Evolution_GroupwiseStorage_AccountConfigControl"

#define GROUPWISE_GCONF_DIR "/apps/evolution-groupwise"
#define GROUPWISE_GCONF_NAME "/apps/evolution-groupwise/account_name"
#define GROUPWISE_GCONF_URI "/apps/evolution-groupwise/account_uri"
#define GROUPWISE_GCONF_USER "/apps/evolution-groupwise/account_user"
#define GROUPWISE_GCONF_PASSWORD "/apps/evolution-groupwise/account_password"

static CalFactory *cal_factory = NULL;
static BonoboGenericFactory *config_factory = NULL;

static GroupwiseAccount *account ;
static GroupwiseStorage *storage;
static EvolutionShellClient *global_shell_client;
static GConfClient *gconf;

static GroupwiseAccount *
account_load (void)
{
	GroupwiseAccount *account = NULL;
	char *name, *uri, *user, *password;
	
	if (!gconf) {
		gconf = gconf_client_get_default ();
		gconf_client_add_dir (gconf, GROUPWISE_GCONF_DIR, GCONF_CLIENT_PRELOAD_RECURSIVE, NULL);
	}

	name = gconf_client_get_string (gconf, GROUPWISE_GCONF_NAME, NULL);
	uri = gconf_client_get_string (gconf, GROUPWISE_GCONF_URI, NULL);
	user = gconf_client_get_string (gconf, GROUPWISE_GCONF_USER, NULL);
	password = gconf_client_get_string (gconf, GROUPWISE_GCONF_PASSWORD, NULL);
	
	if (name && uri && user && password)
		account = groupwise_account_new (name, uri, user, password);

	g_free (name);
	g_free (uri);
	g_free (user);
	g_free (password);
	
	g_object_unref (gconf);

	return account;
}

static void
account_save (GroupwiseAccount *account) 
{
	const char *name, *uri, *user, *password;

	if (!gconf) {
		gconf = gconf_client_get_default ();
		gconf_client_add_dir (gconf, GROUPWISE_GCONF_DIR, GCONF_CLIENT_PRELOAD_RECURSIVE, NULL);
	}

	name = groupwise_account_get_name (account);
	uri = groupwise_account_get_uri (account);
	user = groupwise_account_get_user (account);
	password = groupwise_account_get_password (account);
	
	if (!(name && uri && user && password))
		return;
	
	gconf_client_set_string (gconf, GROUPWISE_GCONF_NAME, name, NULL);
	gconf_client_set_string (gconf, GROUPWISE_GCONF_URI, uri, NULL);
	gconf_client_set_string (gconf, GROUPWISE_GCONF_USER, user, NULL);
	gconf_client_set_string (gconf, GROUPWISE_GCONF_PASSWORD, password, NULL);
	
	g_object_unref (gconf);	
}

static void
owner_set_cb (EvolutionShellComponent *shell_component,
	      EvolutionShellClient *shell_client,
	      const char *evo_dir, gpointer user_data)
{
	account = account_load ();
	global_shell_client = shell_client;

	if (account) {
		storage = groupwise_storage_new (account);	

		evolution_storage_register_on_shell (EVOLUTION_STORAGE (storage), 
						     evolution_shell_client_corba_objref (shell_client));
	}
}

/* This returns %TRUE all the time, so if set as an idle callback it
   effectively causes each and every nested glib mainloop to be quit.  */
static gboolean
quit_cb (gpointer closure)
{
	bonobo_main_quit ();
	return TRUE;
}

static void
owner_unset_cb (EvolutionShellComponent *shell_component,
		gpointer user_data)
{
	soup_shutdown ();
	g_timeout_add (500, quit_cb, NULL);
}

static void
interactive_cb (EvolutionShellComponent *shell_component,
		gboolean on, gulong new_view_xid,
		gpointer user_data)
{
}

static void
last_calendar_gone_cb (CalFactory *factory, gpointer data)
{
	/* FIXME: what to do? */
}

/* Creates the calendar factory object and registers it */
static gboolean
setup_pcs (int argc, char **argv)
{
	cal_factory = cal_factory_new ();
	if (!cal_factory) {
		g_message (G_STRLOC ": Could not create the calendar factory");
		return FALSE;
	}

	cal_factory_register_method (cal_factory, "groupwise", CAL_BACKEND_GROUPWISE_TYPE);

	/* register the factory in OAF */
	if (!cal_factory_oaf_register (cal_factory, GROUPWISE_CALENDAR_FACTORY_ID)) {
		bonobo_object_unref (BONOBO_OBJECT (cal_factory));
		cal_factory = NULL;
		return FALSE;
	}

	g_signal_connect (cal_factory, "last_calendar_gone",
			  G_CALLBACK (last_calendar_gone_cb), NULL);

	return TRUE;
}

static void
control_apply_cb (EvolutionConfigControl *config_control, gpointer data)
{
	GroupwiseAccount *new_account;
	
	new_account = GROUPWISE_ACCOUNT (data);
	account_save (new_account);

	if (!account) {
		account = g_object_ref (new_account);

		storage = groupwise_storage_new (account);
		evolution_storage_register_on_shell (EVOLUTION_STORAGE (storage), 
						     evolution_shell_client_corba_objref (global_shell_client));
	} else {
		GtkWidget *dlg;
		
		dlg = gtk_message_dialog_new (NULL, GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
					      GTK_MESSAGE_QUESTION, GTK_BUTTONS_OK,
					      "You must restart evolution in order for changes to take affect");

		gtk_dialog_run (GTK_DIALOG (dlg));
		gtk_widget_destroy (dlg);
	}
}

static void
config_destroy (gpointer data, GObject *where_object_was) 
{
	GroupwiseAccount *new_account;
	
	new_account = GROUPWISE_ACCOUNT (data);
	
	g_object_unref (new_account);
}

static BonoboObject *
config_factory_func (BonoboGenericFactory *factory,
		     const char *component_id,
		     gpointer data)
{
	g_message ("%s : %s", GROUPWISE_ACCOUNT_CONTROL_ID, component_id);
	
	if (!strcmp (component_id, GROUPWISE_ACCOUNT_CONTROL_ID)) {
		BonoboObject *control;
		GroupwiseAccount *new_account;

		if (account)
			new_account = groupwise_account_new_from_existing (account);
		else
			new_account = groupwise_account_new (NULL, NULL, NULL, NULL);

		control = groupwise_account_control_new (new_account);

		g_signal_connect (control, "apply", G_CALLBACK (control_apply_cb), new_account);
		g_object_weak_ref (G_OBJECT (control), config_destroy, new_account);
		
		return control;
	}
	
	return NULL;
}


static gboolean
setup_config (int argc, char **argv)
{
	config_factory = bonobo_generic_factory_new (
		GROUPWISE_CONFIG_FACTORY_ID, config_factory_func, NULL);

	return config_factory != CORBA_OBJECT_NIL;
}

int
main (int argc, char **argv)
{
	int ret;

	EvolutionShellComponent *shell_component;
	EvolutionShellComponentFolderType types[] = {
		{ "groupwise-server", "none", "groupwise-server", "Groupwise server", FALSE, NULL, NULL },
		{ NULL, NULL, NULL, NULL, FALSE, NULL, NULL }
	};

	bindtextdomain (PACKAGE, LOCALEDIR);
	textdomain (PACKAGE);

	gnome_program_init (PACKAGE, VERSION, LIBGNOMEUI_MODULE, argc, argv,
			    GNOME_PROGRAM_STANDARD_PROPERTIES,
			    GNOME_PARAM_HUMAN_READABLE_NAME, _("Ximian Connector for Groupwise"),
			    NULL);

	gnome_vfs_init ();
	glade_init ();
	
	shell_component = evolution_shell_component_new (
		types, NULL, NULL, NULL, NULL, NULL,
		NULL, NULL, NULL, NULL, NULL);

	g_signal_connect (shell_component, "owner_set",
			  G_CALLBACK (owner_set_cb), NULL);
	g_signal_connect (shell_component, "owner_unset",
			  G_CALLBACK (owner_unset_cb), NULL);
	g_signal_connect (shell_component, "interactive",
			  G_CALLBACK (interactive_cb), NULL);

	/* register factories */
	ret = bonobo_activation_active_server_register (
		GROUPWISE_STORAGE_COMPONENT_ID,
		bonobo_object_corba_objref (BONOBO_OBJECT (shell_component))); 
	if (ret != Bonobo_ACTIVATION_REG_SUCCESS)
		goto failed;

	if (!setup_pcs (argc, argv))
		goto failed;

        if (!setup_config (argc, argv))
		goto failed;

	g_message ("Groupwise connector up and running");
	
	bonobo_main ();

	e_passwords_shutdown ();

	return 0;

 failed:
	g_warning ("\nCould not register Evolution Groupwise backend services.\n"
		   "This probably means another copy of evolution-groupwise-storage\n"
		   "is already running.\n");

	return 1;
}
