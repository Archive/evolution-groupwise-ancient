/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GROUPWISE_STORAGE_H
#define GROUPWISE_STORAGE_H

#include <glib.h>
#include <glib-object.h>
#include <shell/evolution-storage.h>

#include "groupwise-account.h"

G_BEGIN_DECLS



#define GROUPWISE_TYPE_STORAGE            (groupwise_storage_get_type ())
#define GROUPWISE_STORAGE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GROUPWISE_TYPE_STORAGE, GroupwiseStorage))
#define GROUPWISE_STORAGE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GROUPWISE_TYPE_STORAGE,		\
				     GroupwiseStorageClass))
#define GROUPWISE_IS_STORAGE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GROUPWISE_TYPE_STORAGE))
#define GROUPWISE_IS_STORAGE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GROUPWISE_TYPE_STORAGE))

/* Open status values */
typedef struct _GroupwiseStorage GroupwiseStorage;
typedef struct _GroupwiseStorageClass GroupwiseStorageClass;
typedef struct _GroupwiseStoragePrivate GroupwiseStoragePrivate;

struct _GroupwiseStorage {
	EvolutionStorage parent;

	GroupwiseStoragePrivate *priv;
};

struct _GroupwiseStorageClass {
	EvolutionStorageClass parent_class;
};

GType                groupwise_storage_get_type (void);
GroupwiseStorage *groupwise_storage_new (GroupwiseAccount *account);

G_END_DECLS

#endif
