/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GROUPWISE_ACCOUNT_CONTROL_H
#define GROUPWISE_ACCOUNT_CONTROL_H

#include <glib.h>
#include <glib-object.h>
#include <shell/evolution-config-control.h>

#include "groupwise-account.h"

G_BEGIN_DECLS



#define GROUPWISE_TYPE_ACCOUNT_CONTROL            (groupwise_account_control_get_type ())
#define GROUPWISE_ACCOUNT_CONTROL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GROUPWISE_TYPE_ACCOUNT_CONTROL, GroupwiseAccountControl))
#define GROUPWISE_ACCOUNT_CONTROL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GROUPWISE_TYPE_ACCOUNT_CONTROL,		\
				     GroupwiseAccountControlClass))
#define GROUPWISE_IS_ACCOUNT_CONTROL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GROUPWISE_TYPE_ACCOUNT_CONTROL))
#define GROUPWISE_IS_ACCOUNT_CONTROL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GROUPWISE_TYPE_ACCOUNT_CONTROL))

/* Open status values */
typedef struct _GroupwiseAccountControl GroupwiseAccountControl;
typedef struct _GroupwiseAccountControlClass GroupwiseAccountControlClass;
typedef struct _GroupwiseAccountControlPrivate GroupwiseAccountControlPrivate;

struct _GroupwiseAccountControl {
	EvolutionConfigControl parent;

	GroupwiseAccountControlPrivate *priv;
};

struct _GroupwiseAccountControlClass {
	EvolutionConfigControlClass parent_class;
};

GType                groupwise_account_control_get_type (void);
BonoboObject *groupwise_account_control_new (GroupwiseAccount *account);

G_END_DECLS

#endif
