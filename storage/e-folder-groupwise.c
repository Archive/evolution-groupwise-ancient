/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>

#include "e-folder-groupwise.h"

struct _EFolderGroupwisePrivate {
	gboolean has_subfolders;

	char *path;
};

static GObjectClass *parent_class;

static void
e_folder_groupwise_init (EFolderGroupwise *folder)
{
	EFolderGroupwisePrivate *priv;

	priv = g_new0 (EFolderGroupwisePrivate, 1);
	folder->priv = priv;

	priv->has_subfolders = FALSE;
}

static void
e_folder_groupwise_finalize (GObject *object) 
{
	EFolderGroupwise *folder;
	EFolderGroupwisePrivate *priv;
	
	folder = E_FOLDER_GROUPWISE (object);
	priv = folder->priv;
	
	g_free (priv->path);
	
	g_free (priv);
}

static void
e_folder_groupwise_class_init (EFolderGroupwiseClass *klass)
{
	GObjectClass *object_class;
	
	parent_class = (GObjectClass *) g_type_class_peek_parent (klass);

	object_class = (GObjectClass *) klass;

	object_class->finalize = e_folder_groupwise_finalize;
}

GType
e_folder_groupwise_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static GTypeInfo info = {
                        sizeof (EFolderGroupwiseClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) e_folder_groupwise_class_init,
                        NULL, NULL,
                        sizeof (EFolderGroupwise),
                        0,
                        (GInstanceInitFunc) e_folder_groupwise_init,
                };
		type = g_type_register_static (E_TYPE_FOLDER, "EFolderGroupwise", &info, 0);
	}

	return type;
}

EFolderGroupwise *
e_folder_groupwise_new (const char *name, const char *type, const char *description)
{
	EFolderGroupwise *folder;
	
	folder = g_object_new (E_TYPE_FOLDER_GROUPWISE, NULL);

	e_folder_construct (E_FOLDER (folder), name, type, description);
	
	return folder;
}

gboolean
e_folder_groupwise_get_has_subfolders (EFolderGroupwise *folder)
{
	EFolderGroupwisePrivate *priv;
	
	g_return_val_if_fail (folder != NULL, FALSE);
	g_return_val_if_fail (E_IS_FOLDER_GROUPWISE (folder), FALSE);

	priv = folder->priv;
	
	return priv->has_subfolders;
}

void
e_folder_groupwise_set_has_subfolders (EFolderGroupwise *folder, gboolean has_subfolders)
{
	EFolderGroupwisePrivate *priv;

	g_return_if_fail (folder != NULL);
	g_return_if_fail (E_IS_FOLDER_GROUPWISE (folder));

	priv = folder->priv;
	
	priv->has_subfolders = has_subfolders;
}

const char *
e_folder_groupwise_get_path (EFolderGroupwise *folder)
{
	EFolderGroupwisePrivate *priv;

	g_return_val_if_fail (folder != NULL, NULL);
	g_return_val_if_fail (E_IS_FOLDER_GROUPWISE (folder), NULL);

	priv = folder->priv;
	
	return priv->path;
}

void
e_folder_groupwise_set_path (EFolderGroupwise *folder, const char *path)
{
	EFolderGroupwisePrivate *priv;

	g_return_if_fail (folder != NULL);
	g_return_if_fail (E_IS_FOLDER_GROUPWISE (folder));
	g_return_if_fail (path != NULL);

	priv = folder->priv;

	g_free (priv->path);
	priv->path = g_strdup (path);
}



