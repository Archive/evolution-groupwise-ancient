/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>

#include "e-folder-groupwise.h"
#include "groupwise-connection.h"
#include "groupwise-connection-cache.h"
#include "groupwise-util.h"
#include "groupwise-account.h"

struct _GroupwiseAccountPrivate {
	char *name;
	char *uri;
	char *user;
	char *password;

	GHashTable *folders;
	
	GroupwiseConnection *gw;
};

enum props {
	PROP_0,
	PROP_NAME,
	PROP_URI,
	PROP_USER,
	PROP_PASSWORD
};

static GObjectClass *parent_class;

static void
groupwise_account_init (GroupwiseAccount *gw)
{
	GroupwiseAccountPrivate *priv;

	priv = g_new0 (GroupwiseAccountPrivate, 1);
	gw->priv = priv;

	priv->folders = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);
}

static void
groupwise_account_set_property (GObject *object, guint property_id, 
				   const GValue *value, GParamSpec *pspec)
{
	GroupwiseAccount *gw;
	GroupwiseAccountPrivate *priv;
	
	gw = GROUPWISE_ACCOUNT (object);
	priv = gw->priv;
	
	switch (property_id) {
	case PROP_NAME:
		priv->name = g_value_dup_string (value);
		break;
	case PROP_URI:
		priv->uri = g_value_dup_string (value);
		break;
	case PROP_USER:
		priv->user = g_value_dup_string (value);
		break;
	case PROP_PASSWORD:
		priv->password = g_value_dup_string (value);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
groupwise_account_get_property (GObject *object, guint property_id, 
				   GValue *value, GParamSpec *pspec)
{
	GroupwiseAccount *gw;
	GroupwiseAccountPrivate *priv;
	
	gw = GROUPWISE_ACCOUNT (object);
	priv = gw->priv;

	switch (property_id) {
	case PROP_NAME:
		g_value_set_string (value, priv->name);
		break;
	case PROP_URI:
		g_value_set_string (value, priv->uri);
		break;
	case PROP_USER:
		g_value_set_string (value, priv->user);
		break;
	case PROP_PASSWORD:
		g_value_set_string (value, priv->password);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
groupwise_account_finalize (GObject *object) 
{
	GroupwiseAccount *gw;
	GroupwiseAccountPrivate *priv;
	
	gw = GROUPWISE_ACCOUNT (object);
	priv = gw->priv;

	g_free (priv->name);
	g_free (priv->uri);
	g_free (priv->user);
	g_free (priv->password);

	g_hash_table_destroy (priv->folders);
	
	if (priv->gw)
		g_object_unref (priv->gw);
	
	g_free (priv);
}

static void
groupwise_account_class_init (GroupwiseAccountClass *klass)
{
	GObjectClass *object_class;
	GParamSpec *param;
	
	parent_class = (GObjectClass *) g_type_class_peek_parent (klass);

	object_class = (GObjectClass *) klass;

	object_class->get_property = groupwise_account_get_property;
	object_class->set_property = groupwise_account_set_property;
	object_class->finalize = groupwise_account_finalize;

	param = g_param_spec_string ("name", NULL, NULL, "",
				     G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
	g_object_class_install_property (object_class, PROP_NAME, param);

	param = g_param_spec_string ("uri", NULL, NULL, "",
				     G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
	g_object_class_install_property (object_class, PROP_URI, param);

	param = g_param_spec_string ("user", NULL, NULL, "",
				     G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
	g_object_class_install_property (object_class, PROP_USER, param);

	param = g_param_spec_string ("password", NULL, NULL, "",
				     G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
	g_object_class_install_property (object_class, PROP_PASSWORD, param);
}

GType
groupwise_account_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static GTypeInfo info = {
                        sizeof (GroupwiseAccountClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) groupwise_account_class_init,
                        NULL, NULL,
                        sizeof (GroupwiseAccount),
                        0,
                        (GInstanceInitFunc) groupwise_account_init,
                };
		type = g_type_register_static (G_TYPE_OBJECT, "GroupwiseAccount", &info, 0);
	}

	return type;
}

GroupwiseAccount *
groupwise_account_new (const char *name, const char *uri, const char *user, const char *password)
{
	GroupwiseAccount *account;
	
	account = g_object_new (GROUPWISE_TYPE_ACCOUNT, "name", name, "uri", uri, 
				"user", user, "password", password, NULL);

	return account;
}

GroupwiseAccount *
groupwise_account_new_from_existing (GroupwiseAccount *existing_account)
{
	GroupwiseAccount *account;	

	account = g_object_new (GROUPWISE_TYPE_ACCOUNT, 
				"name", groupwise_account_get_name (existing_account), 
				"uri", groupwise_account_get_uri (existing_account), 
				"user", groupwise_account_get_user (existing_account), 
				"password", groupwise_account_get_password (existing_account),
				NULL);

	return account;
}

static void
add_efolder (GroupwiseAccount *account, GroupwiseFolder *folder, const char *type)
{
	GroupwiseAccountPrivate *priv;
	EFolderGroupwise *efolder;
	char *path, *physical_uri;
	
	priv = account->priv;

	/* FIXME We need to handle hierarchal folders */
	path = g_strdup_printf ("/%s-%s", folder->name, type);	
	physical_uri = groupwise_util_httpuri_to_gwuri (priv->uri, folder->id, type);

	efolder = e_folder_groupwise_new (folder->name, type, NULL);
	e_folder_set_physical_uri (E_FOLDER (efolder), physical_uri);
	e_folder_groupwise_set_has_subfolders (efolder, folder->subfolders);
	e_folder_groupwise_set_path (efolder, path);

	g_hash_table_insert (priv->folders, path, efolder);

	g_free (physical_uri);
}

gboolean 
groupwise_account_connect (GroupwiseAccount *account)
{
	GroupwiseAccountPrivate *priv;
	GList *folders, *l;
	
	g_return_val_if_fail (account != NULL, FALSE);
	g_return_val_if_fail (GROUPWISE_IS_ACCOUNT (account), FALSE);

	priv = account->priv;

	if (priv->gw)
		return TRUE;
	
	priv->gw = groupwise_connection_new (priv->uri, priv->user, priv->password);
	
	if (!groupwise_connection_user_login (priv->gw))
		return FALSE;	

	if (!groupwise_connection_folder_list (priv->gw, &folders))
		return FALSE;	

	groupwise_connection_cache_add (priv->uri, priv->gw);

	for (l = folders; l; l = l->next) {
		GroupwiseFolder *folder = l->data;
		
		if (folder->type == GROUPWISE_FOLDER_TYPE_CALENDAR) {
			add_efolder (account, folder, "calendar");
			add_efolder (account, folder, "tasks");
		}
		
		groupwise_connection_folder_free (folder);
	}
	g_list_free (folders);
	
	return TRUE;
}

static int
folder_comparator (const void *a, const void *b)
{
	EFolderGroupwise **fa = (EFolderGroupwise **)a;
	EFolderGroupwise **fb = (EFolderGroupwise **)b;

	return strcmp (e_folder_groupwise_get_path (*fa),
		       e_folder_groupwise_get_path (*fb));
}

static void
add_folder (gpointer key, gpointer value, gpointer folders)
{
	EFolder *folder = value;

	g_ptr_array_add (folders, folder);
}

GPtrArray *
groupwise_account_get_folders (GroupwiseAccount *account)
{
	GroupwiseAccountPrivate *priv;
	GPtrArray *folders;

	g_return_val_if_fail (account != NULL, FALSE);
	g_return_val_if_fail (GROUPWISE_IS_ACCOUNT (account), FALSE);

	priv = account->priv;

	folders = g_ptr_array_new ();
	g_hash_table_foreach (priv->folders, add_folder, folders);

	qsort (folders->pdata, folders->len,
	       sizeof (EFolder *), folder_comparator);

	return folders;
}

const char *
groupwise_account_get_name (GroupwiseAccount *account)
{
	GroupwiseAccountPrivate *priv;
	
	g_return_val_if_fail (account != NULL, NULL);
	g_return_val_if_fail (GROUPWISE_IS_ACCOUNT (account), NULL);

	priv = account->priv;
	
	return priv->name;
}

void
groupwise_account_set_name (GroupwiseAccount *account, const char *name)
{
	GroupwiseAccountPrivate *priv;
	
	g_return_if_fail (account != NULL);
	g_return_if_fail (GROUPWISE_IS_ACCOUNT (account));

	priv = account->priv;
	
	g_free (priv->name);
	priv->name = g_strdup (name);
}

const char *
groupwise_account_get_uri (GroupwiseAccount *account)
{
	GroupwiseAccountPrivate *priv;
	
	g_return_val_if_fail (account != NULL, NULL);
	g_return_val_if_fail (GROUPWISE_IS_ACCOUNT (account), NULL);

	priv = account->priv;
	
	return priv->uri;
}

void
groupwise_account_set_uri (GroupwiseAccount *account, const char *uri)
{
	GroupwiseAccountPrivate *priv;
	
	g_return_if_fail (account != NULL);
	g_return_if_fail (GROUPWISE_IS_ACCOUNT (account));

	priv = account->priv;
	
	g_free (priv->uri);
	priv->uri = g_strdup (uri);
}

const char *
groupwise_account_get_user (GroupwiseAccount *account)
{
	GroupwiseAccountPrivate *priv;
	
	g_return_val_if_fail (account != NULL, NULL);
	g_return_val_if_fail (GROUPWISE_IS_ACCOUNT (account), NULL);

	priv = account->priv;
	
	return priv->user;
}

void
groupwise_account_set_user (GroupwiseAccount *account, const char *user)
{
	GroupwiseAccountPrivate *priv;
	
	g_return_if_fail (account != NULL);
	g_return_if_fail (GROUPWISE_IS_ACCOUNT (account));

	priv = account->priv;
	
	g_free (priv->user);
	priv->user = g_strdup (user);
}

const char *
groupwise_account_get_password (GroupwiseAccount *account)
{
	GroupwiseAccountPrivate *priv;
	
	g_return_val_if_fail (account != NULL, NULL);
	g_return_val_if_fail (GROUPWISE_IS_ACCOUNT (account), NULL);

	priv = account->priv;
	
	return priv->password;
}

void
groupwise_account_set_password (GroupwiseAccount *account, const char *password)
{
	GroupwiseAccountPrivate *priv;
	
	g_return_if_fail (account != NULL);
	g_return_if_fail (GROUPWISE_IS_ACCOUNT (account));

	priv = account->priv;
	
	g_free (priv->password);
	priv->password = g_strdup (password);
}

gboolean 
groupwise_account_is_complete (GroupwiseAccount *account)
{
	GroupwiseAccountPrivate *priv;
	
	g_return_val_if_fail (account != NULL, FALSE);
	g_return_val_if_fail (GROUPWISE_IS_ACCOUNT (account), FALSE);

	priv = account->priv;
	
	return priv->name && priv->uri && priv->user && priv->password;
}
