/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-moniker-util.h>
#include <e-util/e-url.h>
#include <e-util/e-path.h>
#include <e-util/e-xml-hash-utils.h>
#include <gal/util/e-util.h>
#include "cal-backend-groupwise.h"
#include "cal-util/cal-component.h"
#include "cal-util/cal-recur.h"
#include "cal-util/cal-util.h"
#include <libsoup/soup-message.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>
#include <ical.h>

#include "groupwise-connection.h"
#include "groupwise-connection-cache.h"
#include "groupwise-util.h"

/* Private part of the CalBackendGroupwise structure */
struct _CalBackendGroupwisePrivate {
        char *groupwise_uri;
        char *http_uri;

	gboolean is_loaded;
	gboolean writable;
	
	GroupwiseConnection *gw;

	int folder_id;
	icalcomponent_kind folder_type;

	/* list of objects */
	GHashTable *objects;
	GHashTable *drn_to_uid;
	GHashTable *uid_to_drn;
	GHashTable *timezones;
	icaltimezone *default_timezone;
};

static void cal_backend_groupwise_class_init (CalBackendGroupwiseClass *klass);
static void cal_backend_groupwise_init (CalBackendGroupwise *cbgw);
static void cal_backend_groupwise_dispose (GObject *object);
static void cal_backend_groupwise_finalize (GObject *object);

static const char *cal_backend_groupwise_get_uri (CalBackend *backend);
static gboolean cal_backend_groupwise_is_read_only (CalBackend *backend);
static const char * cal_backend_groupwise_get_cal_address (CalBackend *backend);

static const char *cal_backend_groupwise_get_alarm_email_address (CalBackend *backend);
static const char *cal_backend_groupwise_get_ldap_attribute (CalBackend *backend);
static const char *cal_backend_groupwise_get_static_capabilities (CalBackend *backend);

static CalBackendOpenStatus cal_backend_groupwise_open (CalBackend *backend,
						       const char *uristr,
						       gboolean only_if_exists);
static gboolean cal_backend_groupwise_is_loaded (CalBackend *backend);

static CalMode cal_backend_groupwise_get_mode (CalBackend *backend);
static void cal_backend_groupwise_set_mode (CalBackend *backend, CalMode mode);

static char *cal_backend_groupwise_get_default_object (CalBackend *backend, CalObjType type);

static int cal_backend_groupwise_get_n_objects (CalBackend *backend, CalObjType type);
static CalComponent *cal_backend_groupwise_get_object_component (CalBackend *backend, const char *uid);
static char *cal_backend_groupwise_get_timezone_object (CalBackend *backend, const char *tzid);
static GList *cal_backend_groupwise_get_uids (CalBackend *backend, CalObjType type);
static GList *cal_backend_groupwise_get_objects_in_range (CalBackend *backend, CalObjType type,
							 time_t start, time_t end);
static GList *cal_backend_groupwise_get_free_busy (
	CalBackend *backend, GList *users, time_t start, time_t end);
static GNOME_Evolution_Calendar_CalObjChangeSeq *cal_backend_groupwise_get_changes (
	CalBackend *backend, CalObjType type, const char *change_id);

static GNOME_Evolution_Calendar_CalComponentAlarmsSeq *
cal_backend_groupwise_get_alarms_in_range (CalBackend *backend, time_t start, time_t end);

static GNOME_Evolution_Calendar_CalComponentAlarms *
cal_backend_groupwise_get_alarms_for_object (CalBackend *backend, const char *uid,
					    time_t start, time_t end,
					    gboolean *object_found);

static CalBackendResult cal_backend_groupwise_discard_alarm (CalBackend *backend,
							    const char *uid,
							    const char *auid);

static CalBackendResult cal_backend_groupwise_update_objects (CalBackend *backend,
							     const char *calobj,
							     CalObjModType mod);
static CalBackendResult cal_backend_groupwise_remove_object (CalBackend *backend,
							    const char *uid,
							    CalObjModType mod);

static CalBackendSendResult cal_backend_groupwise_send_object (CalBackend *backend, 
							      const char *calobj, char **new_calobj,
							      GNOME_Evolution_Calendar_UserList **user_list,
							      char error_msg[256]);

static icaltimezone* cal_backend_groupwise_get_timezone (CalBackend *backend, const char *tzid);
static icaltimezone* cal_backend_groupwise_get_default_timezone (CalBackend *backend);
static gboolean cal_backend_groupwise_set_default_timezone (CalBackend *backend,
							   const char *tzid);

#define PARENT_TYPE CAL_BACKEND_TYPE
static CalBackendClass *parent_class = NULL;

static CalComponent *
comp_from_item (icalcomponent_kind folder_type, GroupwiseItem *item, icaltimezone *default_zone) 
{
	CalComponent *comp;
	char *uid;
	CalComponentText text;
	CalComponentDateTime dt;
	struct icaltimetype itt;
	static icaltimezone *utc_zone = NULL;

	if (!utc_zone)
		utc_zone = icaltimezone_get_utc_timezone ();

	comp = cal_component_new ();
	if (folder_type == ICAL_VEVENT_COMPONENT)
		cal_component_set_new_vtype (comp, CAL_COMPONENT_EVENT);
	else if (folder_type == ICAL_VTODO_COMPONENT)
		cal_component_set_new_vtype (comp, CAL_COMPONENT_TODO);
	else
		return NULL;
		
	/* UID */
	uid = cal_component_gen_uid ();
	cal_component_set_uid (comp, uid);
	g_free (uid);
	
	/* Summary */
	text.value = item->subject;
	text.altrep = NULL;
	cal_component_set_summary (comp, &text);
		
	/* Location */
	cal_component_set_location (comp, item->location);
		
	/* Start/End Time */
	itt.year = item->start.year;
	itt.month = item->start.month;
	itt.day = item->start.day;
	itt.hour = item->start.am ? item->start.hour % 12 : 
		item->start.hour == 12 ? item->start.hour : item->start.hour + 12;
	itt.minute = item->start.minute;
	itt.second = 0;
	itt.is_utc = 1;
	itt.is_date = 0;
	itt.is_daylight = 0;
	itt.zone = NULL;

	icaltimezone_convert_time (&itt, utc_zone, default_zone);	

	dt.value = &itt;
	dt.tzid = icaltimezone_get_tzid (default_zone);

	cal_component_set_dtstart (comp, &dt);
		
	switch (item->duration.type) {
	case GROUPWISE_DURATION_TYPE_DAYS:
		icaltime_adjust (&itt, item->duration.length, 0, 0, 0);
		break;
	case GROUPWISE_DURATION_TYPE_HOURS:
		icaltime_adjust (&itt, 0, item->duration.length, 0, 0);
		break;		
	case GROUPWISE_DURATION_TYPE_MINUTES:
		icaltime_adjust (&itt, 0, 0, item->duration.length, 0);
		break;
	default:
	}
	cal_component_set_dtend (comp, &dt);

	cal_component_commit_sequence (comp);

	return comp;
}

static GroupwiseItem *
item_from_comp (CalComponent *comp, GroupwiseConnection *gw, GHashTable *uid_to_drn)
{
	GroupwiseItem *item;
	CalComponentText text;
	CalComponentDateTime dtstart, dtend;
	const char *location, *drn, *uid;
	struct icaltimetype itt_start, itt_end;
	time_t start, end;
	
	item = g_new0 (GroupwiseItem, 1);

	/* Type */
	if (cal_component_get_vtype (comp) == CAL_COMPONENT_EVENT)
		item->type = GROUPWISE_ITEM_TYPE_APPOINTMENT;
	else
		item->type = GROUPWISE_ITEM_TYPE_TASK;

	/* DRN */
	cal_component_get_uid (comp, &uid);
	drn = g_hash_table_lookup (uid_to_drn, uid);
	if (drn)
		item->drn = g_strdup (drn);
	
	/* To */
	if (cal_component_has_attendees (comp)) {
		GSList *attendees, *l;
		GString *recipients;

		recipients = g_string_new (NULL);
		
		cal_component_get_attendee_list (comp, &attendees);
		for (l = attendees; l; l = l->next) {
			CalComponentAttendee *a = l->data;
			
			if (recipients->len > 0)
				g_string_append_printf (recipients, ", %s", a->value);
			else
				g_string_append (recipients, a->value);
		}
		cal_component_free_attendee_list (attendees);
		
		item->to = recipients->str;
		g_string_free (recipients, FALSE);
	} else {
		const char *user;
		
		/* FIXME - Should this be in the default object */
		/* FIXME - Is the user name equal to the email address always? */

		g_object_get (G_OBJECT (gw), "user", &user, NULL);
		item->to = g_strdup (user);
	}	
	
	/* Subject */
	cal_component_get_summary (comp, &text);
	item->subject = g_strdup (text.value);
	
	/* Location */
	cal_component_get_location (comp, &location);
	item->location = g_strdup (location);

	/* Start time and duration */
	cal_component_get_dtstart (comp, &dtstart);
	cal_component_get_dtend (comp, &dtend);

	itt_start = *(dtstart.value);
	itt_end = *(dtend.value);
	
	start = icaltime_as_timet_with_zone (*(dtstart.value), icaltimezone_get_builtin_timezone_from_tzid (dtstart.tzid));
	end = icaltime_as_timet_with_zone (*(dtend.value), icaltimezone_get_builtin_timezone_from_tzid (dtend.tzid));
	
	item->start.year = itt_start.year;
	item->start.month = itt_start.month;
	item->start.day = itt_start.day;
	item->start.hour = itt_start.hour % 12;
	if (item->start.hour == 0)
		item->start.hour = 12;
	item->start.minute = itt_start.minute;
	item->start.am = itt_start.hour < 12 ? TRUE : FALSE;	

	if ((end - start) % (60 * 60 * 24) == 0) {
		/* Evenly divisible by whole days */
		item->duration.type = GROUPWISE_DURATION_TYPE_DAYS;
		item->duration.length = (end - start) / (60 * 60 * 24);
	} else if ((end - start) % (60 * 60) == 0) {
		/* Evenly divisible by whole hours */
		item->duration.type = GROUPWISE_DURATION_TYPE_HOURS;
		item->duration.length = (end - start) / (60 * 60);
	} else {
		item->duration.type = GROUPWISE_DURATION_TYPE_MINUTES;
		item->duration.length = (end - start) / (60);
	}

	return item;
}

static void
hash_zone_free (gpointer data) 
{
	icaltimezone *zone = data;
	icaltimezone_free (zone, TRUE);
}

/* Class initialization function for the Groupwise backend */
static void
cal_backend_groupwise_class_init (CalBackendGroupwiseClass *klass)
{
	GObjectClass *object_class;
	CalBackendClass *backend_class;

	object_class = G_OBJECT_CLASS (klass);
	backend_class = CAL_BACKEND_CLASS (klass);

	parent_class = g_type_class_ref (CAL_BACKEND_TYPE);

	object_class->dispose = cal_backend_groupwise_dispose;
	object_class->finalize = cal_backend_groupwise_finalize;

	backend_class->get_uri = cal_backend_groupwise_get_uri;
	backend_class->is_read_only = cal_backend_groupwise_is_read_only;
	backend_class->get_cal_address = cal_backend_groupwise_get_cal_address;
	backend_class->get_alarm_email_address = cal_backend_groupwise_get_alarm_email_address;
	backend_class->get_ldap_attribute = cal_backend_groupwise_get_ldap_attribute;
	backend_class->get_static_capabilities = cal_backend_groupwise_get_static_capabilities;
	backend_class->open = cal_backend_groupwise_open;
	backend_class->is_loaded = cal_backend_groupwise_is_loaded;
	backend_class->get_mode = cal_backend_groupwise_get_mode;
	backend_class->set_mode = cal_backend_groupwise_set_mode;
	backend_class->get_default_object = cal_backend_groupwise_get_default_object;
	backend_class->get_n_objects = cal_backend_groupwise_get_n_objects;
	backend_class->get_object_component = cal_backend_groupwise_get_object_component;
	backend_class->get_timezone_object = cal_backend_groupwise_get_timezone_object;
	backend_class->get_uids = cal_backend_groupwise_get_uids;
	backend_class->get_objects_in_range = cal_backend_groupwise_get_objects_in_range;
	backend_class->get_free_busy = cal_backend_groupwise_get_free_busy;
	backend_class->get_changes = cal_backend_groupwise_get_changes;
	backend_class->get_alarms_in_range = cal_backend_groupwise_get_alarms_in_range;
	backend_class->get_alarms_for_object = cal_backend_groupwise_get_alarms_for_object;
	backend_class->discard_alarm = cal_backend_groupwise_discard_alarm;
	backend_class->update_objects = cal_backend_groupwise_update_objects;
	backend_class->remove_object = cal_backend_groupwise_remove_object;
	backend_class->send_object = cal_backend_groupwise_send_object;
	backend_class->get_timezone = cal_backend_groupwise_get_timezone;
	backend_class->get_default_timezone = cal_backend_groupwise_get_default_timezone;
	backend_class->set_default_timezone = cal_backend_groupwise_set_default_timezone;
}

/* Object initialization function for the Groupwise backend */
static void
cal_backend_groupwise_init (CalBackendGroupwise *cbgw)
{
	cbgw->priv = g_new0 (CalBackendGroupwisePrivate, 1);

	cbgw->priv->is_loaded = FALSE;
	
	cbgw->priv->objects = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);
	cbgw->priv->drn_to_uid = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);
	cbgw->priv->uid_to_drn = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);
	cbgw->priv->timezones = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, hash_zone_free);
}

/* dispose handler for the Groupwise backend */
static void
cal_backend_groupwise_dispose (GObject *object)
{
	CalBackendGroupwise *cbgw;

	g_return_if_fail (CAL_IS_BACKEND_GROUPWISE (object));

	cbgw = CAL_BACKEND_GROUPWISE (object);

	if (G_OBJECT_CLASS (parent_class)->dispose)
		(* G_OBJECT_CLASS (parent_class)->dispose) (object);
}

/* finalize handler for the Groupwise backend */
static void
cal_backend_groupwise_finalize (GObject *object)
{
	CalBackendGroupwise *cbgw;
	CalBackendGroupwisePrivate *priv;
	
	g_return_if_fail (CAL_IS_BACKEND_GROUPWISE (object));

	cbgw = CAL_BACKEND_GROUPWISE (object);
	priv = cbgw->priv;
	
	if (priv->gw)
		g_object_unref (cbgw->priv->gw);
	
	if (priv->groupwise_uri)
		g_free (cbgw->priv->groupwise_uri);

	g_hash_table_destroy (priv->objects);
	g_hash_table_destroy (priv->drn_to_uid);
	g_hash_table_destroy (priv->uid_to_drn);
	g_hash_table_destroy (priv->timezones);
	
	g_free (cbgw->priv);

	if (G_OBJECT_CLASS (parent_class)->finalize)
		(* G_OBJECT_CLASS (parent_class)->finalize) (object);
}

GType
cal_backend_groupwise_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static GTypeInfo info = {
                        sizeof (CalBackendGroupwiseClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) cal_backend_groupwise_class_init,
                        NULL, NULL,
                        sizeof (CalBackendGroupwise),
                        0,
                        (GInstanceInitFunc) cal_backend_groupwise_init
                };
		type = g_type_register_static (CAL_BACKEND_TYPE, "CalBackendGroupwise", &info, 0);
	}

	return type;
}

/* get_uri handler for the Groupwise backend */
static const char *
cal_backend_groupwise_get_uri (CalBackend *backend)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;

	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), NULL);
	return cbgw->priv->groupwise_uri;
}

/* is_read_only handler for the Groupwise backend */
static gboolean
cal_backend_groupwise_is_read_only (CalBackend *backend)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;

	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), FALSE);

	return !cbgw->priv->writable;
}

/* get_cal_address handler for the Groupwise backend */
static const char *
cal_backend_groupwise_get_cal_address (CalBackend *backend)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;

	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), NULL);

	return "";
}

/* get_alarm_email_address handler for the Groupwise backend */
static const char *
cal_backend_groupwise_get_alarm_email_address (CalBackend *backend)
{
	/* We don't support email alarms */
	return NULL;
}

static const char *
cal_backend_groupwise_get_ldap_attribute (CalBackend *backend)
{
	return NULL;
}

static const char *
cal_backend_groupwise_get_static_capabilities (CalBackend *backend)
{
	return CAL_STATIC_CAPABILITY_NO_EMAIL_ALARMS "," \
		CAL_STATIC_CAPABILITY_NO_TASK_ASSIGNMENT "," \
		CAL_STATIC_CAPABILITY_REMOVE_ALARMS;
}

/* open handler for the Groupwise backend */
static CalBackendOpenStatus
cal_backend_groupwise_open (CalBackend *backend, const char *uristr, gboolean only_if_exists)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;
	CalBackendGroupwisePrivate *priv;
	GList *item_info, *l;
	GroupwiseItem *item;
	char *ft;
	
	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), CAL_BACKEND_OPEN_ERROR);
	g_return_val_if_fail (uristr != NULL, CAL_BACKEND_OPEN_ERROR);

	priv = cbgw->priv;
	
	/* Get the URI's */
	priv->groupwise_uri = g_strdup (uristr);	
	priv->http_uri = groupwise_util_gwuri_to_httpuri (uristr, &priv->folder_id, &ft);
	if (!strcmp (ft, "calendar"))
		priv->folder_type = ICAL_VEVENT_COMPONENT;
	else
		priv->folder_type = ICAL_VTODO_COMPONENT;
	g_free (ft);
	
	priv->gw = groupwise_connection_cache_find (priv->http_uri);
	if (!priv->gw)
		return CAL_BACKEND_OPEN_ERROR;	
	g_object_ref (priv->gw);
	
	if (!groupwise_connection_folder_open (priv->gw, priv->folder_id, &item_info))
		return CAL_BACKEND_OPEN_ERROR;		

	for (l = item_info; l; l = l->next) {
		GroupwiseItemInfo *info = l->data;
		CalComponent *comp;
		const char *uid;
		
		if (!groupwise_connection_item_read (priv->gw, info, &item))
			continue;		

 		comp = comp_from_item (priv->folder_type, item, priv->default_timezone);
		if (!comp)
			continue;		
		
		cal_component_get_uid (comp, &uid);
		
		g_hash_table_insert (priv->objects, g_strdup (uid), comp);
		g_hash_table_insert (priv->drn_to_uid, g_strdup (item->drn), g_strdup (uid));
		g_hash_table_insert (priv->uid_to_drn, g_strdup (uid), g_strdup (item->drn));
		
		groupwise_connection_item_free (item);
		groupwise_connection_item_info_free (info);
	}	
	g_list_free (item_info);

	priv->is_loaded = TRUE;
	priv->writable = TRUE;
	
	return CAL_BACKEND_OPEN_SUCCESS;
}

/* is_loaded handler for the Groupwise backend */
static gboolean
cal_backend_groupwise_is_loaded (CalBackend *backend)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;
	CalBackendGroupwisePrivate *priv;
	
	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), FALSE);

	priv = cbgw->priv;
	
	return priv->is_loaded;
}

/* get_mode handler for the Groupwise backend */
static CalMode
cal_backend_groupwise_get_mode (CalBackend *backend)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;

	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), CAL_MODE_INVALID);

	return CAL_MODE_LOCAL;
}

/* set_mode handler for the Groupwise backend */
static void
cal_backend_groupwise_set_mode (CalBackend *backend, CalMode mode)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;

	g_return_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw));

	if (mode == CAL_MODE_REMOTE)
		cal_backend_notify_mode (backend, GNOME_Evolution_Calendar_Listener_MODE_NOT_SUPPORTED, CAL_MODE_LOCAL);
	else
		cal_backend_notify_mode (backend, GNOME_Evolution_Calendar_Listener_MODE_SET, CAL_MODE_LOCAL);
}

/* get_default_object handler for the Groupwise backend */
static char *
cal_backend_groupwise_get_default_object (CalBackend *backend, CalObjType type)
{
	CalComponent *comp;
	char *calobj;
	
	comp = cal_component_new ();
	
	switch (type) {
	case CALOBJ_TYPE_EVENT:
		cal_component_set_new_vtype (comp, CAL_COMPONENT_EVENT);
		break;
	case CALOBJ_TYPE_TODO:
		cal_component_set_new_vtype (comp, CAL_COMPONENT_TODO);
		break;
	default:
		g_object_unref (comp);
		return NULL;
	}

	calobj = cal_component_get_as_string (comp);
	g_object_unref (comp);

	return calobj;
}

/* get_n_objects handler for the Groupwise backend */
static int
cal_backend_groupwise_get_n_objects (CalBackend *backend, CalObjType type)
{
	CalBackendGroupwise *cbgw;
	CalBackendGroupwisePrivate *priv;
	
	g_return_val_if_fail (backend != NULL, -1);
	g_return_val_if_fail (IS_CAL_BACKEND (backend), -1);

	cbgw = CAL_BACKEND_GROUPWISE (backend);
	priv = cbgw->priv;
	
	if (type == CALOBJ_TYPE_EVENT && priv->folder_type == GROUPWISE_FOLDER_TYPE_CALENDAR)
		return g_hash_table_size (priv->objects);
	else if (type == CALOBJ_TYPE_TODO && priv->folder_type == GROUPWISE_FOLDER_TYPE_CHECKLIST)
		return g_hash_table_size (priv->objects);

	return 0;
}

/* get_object_component handler for the Groupwise backend */
static CalComponent *
cal_backend_groupwise_get_object_component (CalBackend *backend, const char *uid)
{
	CalBackendGroupwise *cbgw;
	CalBackendGroupwisePrivate *priv;

	g_return_val_if_fail (backend != NULL, NULL);
	g_return_val_if_fail (IS_CAL_BACKEND (backend), NULL);

	cbgw = CAL_BACKEND_GROUPWISE (backend);
	priv = cbgw->priv;
	
	return g_hash_table_lookup (priv->objects, uid);
}

/* get_timezone_object handler for the Groupwise backend */
static char *
cal_backend_groupwise_get_timezone_object (CalBackend *backend, const char *tzid)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;

	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), NULL);

	return g_strdup ("");
}

static void 
add_uid (gpointer key, gpointer value, gpointer data)
{
	CalComponent *comp = value;
	GList **list = data;
	const char *uid;
	
	cal_component_get_uid (comp, &uid);
	
	*list = g_list_prepend (*list, g_strdup (uid));
}

/* get_uids handler for the Groupwise backend */
static GList *
cal_backend_groupwise_get_uids (CalBackend *backend, CalObjType type)
{
	CalBackendGroupwise *cbgw;
	CalBackendGroupwisePrivate *priv;
	GList *list = NULL;

	g_return_val_if_fail (backend != NULL, NULL);
	g_return_val_if_fail (IS_CAL_BACKEND (backend), NULL);

	cbgw = CAL_BACKEND_GROUPWISE (backend);
	priv = cbgw->priv;

	g_hash_table_foreach (priv->objects, add_uid, &list);
	
	return list;
}

/* get_objects_in_range handler for the Groupwise backend */
static GList *
cal_backend_groupwise_get_objects_in_range (CalBackend *backend,
					   CalObjType type,
					   time_t start,
					   time_t end)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;
	GList *uidlist = NULL;

	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), NULL);
	g_return_val_if_fail (start != -1 && end != -1, NULL);
	g_return_val_if_fail (start <= end, NULL);

	return uidlist;
}

/* get_free_busy handler for the Groupwise backend */
static GList *
cal_backend_groupwise_get_free_busy (CalBackend *backend, GList *users, time_t start, time_t end)
{	
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;
	GList *obj_list = NULL;

	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), NULL);

	return obj_list;
}

/* get_changes handler for the Groupwise backend */
static GNOME_Evolution_Calendar_CalObjChangeSeq *
cal_backend_groupwise_get_changes (CalBackend *backend, CalObjType type, const char *change_id)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;
	GNOME_Evolution_Calendar_CalObjChangeSeq *seq;

	seq = GNOME_Evolution_Calendar_CalObjChangeSeq__alloc ();
	seq->_length = 0;
	seq->_buffer = CORBA_sequence_GNOME_Evolution_Calendar_CalObjChange_allocbuf (0);
	CORBA_sequence_set_release (seq, TRUE);

	return seq;
}

/* get_alarms_in_range handler for the Groupwise backend */
static GNOME_Evolution_Calendar_CalComponentAlarmsSeq *
cal_backend_groupwise_get_alarms_in_range (CalBackend *backend, time_t start, time_t end)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;
	GNOME_Evolution_Calendar_CalComponentAlarmsSeq *seq;

	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), NULL);
	g_return_val_if_fail (start != -1 && end != -1, NULL);
	g_return_val_if_fail (start <= end, NULL);

	/* create the CORBA sequence */
	seq = GNOME_Evolution_Calendar_CalComponentAlarmsSeq__alloc ();
	CORBA_sequence_set_release (seq, TRUE);
	seq->_length = 0;
	seq->_buffer = CORBA_sequence_GNOME_Evolution_Calendar_CalComponentAlarms_allocbuf (0);

	return seq;
}

/* get_alarms_for_object handler for the Groupwise backend */
static GNOME_Evolution_Calendar_CalComponentAlarms *
cal_backend_groupwise_get_alarms_for_object (CalBackend *backend,
					    const char *uid,
					    time_t start,
					    time_t end,
					    gboolean *object_found)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;
	GNOME_Evolution_Calendar_CalComponentAlarms *corba_alarms;

	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), NULL);

	corba_alarms = GNOME_Evolution_Calendar_CalComponentAlarms__alloc ();

        return corba_alarms;
}

/* discard_alarm handler for the Groupwise backend */
static CalBackendResult
cal_backend_groupwise_discard_alarm (CalBackend *backend, const char *uid, const char *auid)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;
	CalBackendResult result = CAL_BACKEND_RESULT_SUCCESS;

	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), CAL_BACKEND_RESULT_NOT_FOUND);
	g_return_val_if_fail (uid != NULL, CAL_BACKEND_RESULT_NOT_FOUND);
	g_return_val_if_fail (auid != NULL, CAL_BACKEND_RESULT_NOT_FOUND);

	return result;
}

static void
update_component (CalBackendGroupwise *cbgw, icalcomponent *icalcomp) 
{
	CalBackendGroupwisePrivate *priv;
	CalComponent *comp;
	GroupwiseItem *item;
	const char *uid;
	char *compose_id = NULL;

	priv = cbgw->priv;
			
	comp = cal_component_new ();
	cal_component_set_icalcomponent (comp, icalcomponent_new_clone (icalcomp));
	cal_component_get_uid (comp, &uid);
	
	item = item_from_comp (comp, priv->gw, priv->uid_to_drn);
	
	/* If it exists already we must delete it and then create it again */
	if (item->drn) {
		groupwise_connection_item_delete (priv->gw, item->drn);

		g_free (item->drn);
		item->drn = NULL;
	}

	/* Compose it */
	if (groupwise_connection_compose (priv->gw, item->type, &compose_id)) {
		if (groupwise_connection_compose_send (priv->gw, compose_id, item, priv->folder_id)) {
			g_hash_table_insert (priv->objects, g_strdup (uid), comp);
			g_hash_table_insert (priv->drn_to_uid, g_strdup (item->drn), g_strdup (uid));
			g_hash_table_insert (priv->uid_to_drn, g_strdup (uid), g_strdup (item->drn));

			cal_backend_notify_update (CAL_BACKEND (cbgw), uid);
		}
	}
	
	g_free (compose_id);			
	groupwise_connection_item_free (item);
	g_object_unref (comp);
}

static void
add_timezone (CalBackendGroupwise *cbgw, icalcomponent *vtzcomp)
{
	icaltimezone *tz;
	icalcomponent *new_vtzcomp;
	char *tzid;

	g_return_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw));
	g_return_if_fail (vtzcomp != NULL);

	tz = icaltimezone_new ();
	new_vtzcomp = icalcomponent_new_clone (vtzcomp);
	if (!icaltimezone_set_component (tz, new_vtzcomp)) {
		icaltimezone_free (tz, TRUE);
		icalcomponent_free (new_vtzcomp);
		return;
	}

	tzid = icaltimezone_get_tzid (tz);
	if (!g_hash_table_lookup (cbgw->priv->timezones, tzid))
		g_hash_table_insert (cbgw->priv->timezones, g_strdup (tzid), tz);
	else
		icaltimezone_free (tz, TRUE);
}

/* update_object handler for the Groupwise backend */
static CalBackendResult
cal_backend_groupwise_update_objects (CalBackend *backend, const char *calobj,
				      CalObjModType mod)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;
	CalBackendGroupwisePrivate *priv;
	icalcomponent *icalcomp;
	icalcomponent_kind kind;

	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), CAL_BACKEND_RESULT_INVALID_OBJECT);
	g_return_val_if_fail (calobj != NULL, CAL_BACKEND_RESULT_INVALID_OBJECT);

	priv = cbgw->priv;
	
	if (!priv->writable) {
		g_warning ("Folder is not writable");
		return CAL_BACKEND_RESULT_PERMISSION_DENIED;
	}

	/* check the component in the string */
	icalcomp = icalparser_parse_string (calobj);
	if (!icalcomp)
		return CAL_BACKEND_RESULT_INVALID_OBJECT;

	kind = icalcomponent_isa (icalcomp);
	if (kind == ICAL_VCALENDAR_COMPONENT) {
		icalcomponent *subcomp;
		icalcomponent *vcalendar_comp;
		icalproperty *method_prop;
		icalproperty_method method_value;

		vcalendar_comp = icalcomp;
		method_prop = icalcomponent_get_first_property (vcalendar_comp,
								ICAL_METHOD_PROPERTY);
		if (method_prop)
			method_value = icalproperty_get_method (method_prop);
		else
			method_value = ICAL_METHOD_REQUEST;

		/* traverse all timezones */
		subcomp = icalcomponent_get_first_component (vcalendar_comp, ICAL_VTIMEZONE_COMPONENT);
		while (subcomp) {
			add_timezone (cbgw, subcomp);
			subcomp = icalcomponent_get_next_component (vcalendar_comp, ICAL_VTIMEZONE_COMPONENT);
		}

		/* traverse all sub-components */
		subcomp = icalcomponent_get_first_component (vcalendar_comp, priv->folder_type);
		while (subcomp) {
			update_component (cbgw, subcomp);

			subcomp = icalcomponent_get_next_component (vcalendar_comp, priv->folder_type);
		}

		icalcomponent_free (icalcomp);
	} else if (kind == priv->folder_type) {
		update_component (cbgw, icalcomp);
	} else {
		icalcomponent_free (icalcomp);
	}
	
	return CAL_BACKEND_RESULT_SUCCESS;
}

/* remove_object handler for the Groupwise backend */
static CalBackendResult
cal_backend_groupwise_remove_object (CalBackend *backend, const char *uid,
				    CalObjModType mod)
{
	CalBackendGroupwise *cbgw;
	CalBackendGroupwisePrivate *priv;
	char *drn;
	
	g_return_val_if_fail (backend != NULL, -1);
	g_return_val_if_fail (IS_CAL_BACKEND (backend), -1);

	cbgw = CAL_BACKEND_GROUPWISE (backend);
	priv = cbgw->priv;

	drn = g_hash_table_lookup (priv->uid_to_drn, uid);
	if (!drn)
		return CAL_BACKEND_RESULT_NOT_FOUND;

	if (!groupwise_connection_item_delete (priv->gw, drn))
		return CAL_BACKEND_RESULT_INVALID_OBJECT;
	
	cal_backend_notify_remove (CAL_BACKEND (cbgw), uid);

	return CAL_BACKEND_RESULT_SUCCESS;
}

static CalBackendSendResult
cal_backend_groupwise_send_object (CalBackend *backend, const char *calobj, char **new_calobj,
				  GNOME_Evolution_Calendar_UserList **user_list, char error_msg[256])
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;

	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), CAL_BACKEND_SEND_INVALID_OBJECT);

	*new_calobj = g_strdup (calobj);
	return CAL_BACKEND_SEND_SUCCESS;
}

/* get_timezone handler for the Groupwise backend */
static icaltimezone *
cal_backend_groupwise_get_timezone (CalBackend *backend, const char *tzid)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;
	icaltimezone *izone;

	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), NULL);

	izone = g_hash_table_lookup (cbgw->priv->timezones, tzid);
	if (!izone)
		izone = icaltimezone_get_builtin_timezone_from_tzid (tzid);

	return izone;
}

/* get_default_timezone handler for the Groupwise backend */
static icaltimezone *
cal_backend_groupwise_get_default_timezone (CalBackend *backend)
{
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;

	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), FALSE);

	return cbgw->priv->default_timezone;
}

/* set_default_timezone handler for the Groupwise backend */
static gboolean
cal_backend_groupwise_set_default_timezone (CalBackend *backend, const char *id)
{
	icaltimezone *zone;
	CalBackendGroupwise *cbgw = (CalBackendGroupwise *) backend;

	g_return_val_if_fail (CAL_IS_BACKEND_GROUPWISE (cbgw), FALSE);

	zone = cal_backend_groupwise_get_timezone (backend, id);
	if (!zone)
		return FALSE;

	cbgw->priv->default_timezone = zone;

	return TRUE;
}
