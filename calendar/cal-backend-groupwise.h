/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CAL_BACKEND_GROUPWISE_H
#define CAL_BACKEND_GROUPWISE_H

#include <pcs/cal-backend.h>
#include <pcs/cal-backend-util.h>

G_BEGIN_DECLS

#define CAL_BACKEND_GROUPWISE_TYPE            (cal_backend_groupwise_get_type ())
#define CAL_BACKEND_GROUPWISE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CAL_BACKEND_GROUPWISE_TYPE,	CalBackendGroupwise))
#define CAL_BACKEND_GROUPWISE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), CAL_BACKEND_GROUPWISE_TYPE,	CalBackendGroupwiseClass))
#define CAL_IS_BACKEND_GROUPWISE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CAL_BACKEND_GROUPWISE_TYPE))
#define CAL_IS_BACKEND_GROUPWISE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), CAL_BACKEND_GROUPWISE_TYPE))

typedef struct _CalBackendGroupwise        CalBackendGroupwise;
typedef struct _CalBackendGroupwiseClass   CalBackendGroupwiseClass;

typedef struct _CalBackendGroupwisePrivate CalBackendGroupwisePrivate;

struct _CalBackendGroupwise {
	CalBackend backend;

	/* Private data */
	CalBackendGroupwisePrivate *priv;
};

struct _CalBackendGroupwiseClass {
	CalBackendClass parent_class;
};

GType   cal_backend_groupwise_get_type (void);

G_END_DECLS

#endif
