/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>

#include <e-util/e-url.h>

#include "groupwise-util.h"

static char *
get_param (const char *uri, const char *name)
{
	char *s;
	char **arr;
	char *param = NULL;

	s = strchr (uri, '?');
	if (!s || !*s)
		return NULL;

	arr = g_strsplit (s + 1, "&", 0);
	if (arr) {
		gint n = 0;

		while (arr[n]) {
			if (!strncmp (arr[n], name, strlen (name))) {
				param = strchr (arr[n], '=');
				if (param) {
					param = g_strdup (param + 1);
					break;
				}
			}

			n++;
		}

		g_strfreev (arr);
	}

	return param;
}

char *
groupwise_util_gwuri_to_httpuri (const char *gwuri, int *folder_id, char **folder_type)
{
	EUri *euri;
	char *retval;
	
	euri = e_uri_new (gwuri);
	if (!euri)
		return NULL;
	
	g_free (euri->protocol);
	g_free (euri->path);
	g_free (euri->query);
	euri->protocol = g_strdup ("http");
	euri->path = NULL;
	euri->query = NULL;
	
	if (folder_id) {
		char *id;
		
		id = get_param (gwuri, "folder_id");
		*folder_id = atoi (id ? id : "");
		g_free (id);
	}
	if (folder_type)
		*folder_type = get_param (gwuri, "folder_type");
	
	retval = e_uri_to_string (euri, FALSE);
	
	e_uri_free (euri);

	return retval;
}

char *
groupwise_util_httpuri_to_gwuri (const char *httpuri, int folder_id, const char *folder_type)
{

	EUri *euri;
	char *retval;
	
	euri = e_uri_new (httpuri);
	if (!euri)
		return NULL;
	
	g_free (euri->protocol);
	g_free (euri->path);
	g_free (euri->query);
	euri->protocol = g_strdup ("groupwise");
	euri->path = g_strdup_printf ("/");
	
	euri->query = g_strdup_printf ("folder_id=%d&folder_type=%s", folder_id, folder_type);

	retval = e_uri_to_string (euri, FALSE);

	e_uri_free (euri);

	return retval;
}


