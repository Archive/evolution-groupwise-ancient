/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <time.h>
#include <glib.h>
#include <libgnome/gnome-util.h>
#include "groupwise-connection.h"

#define GW_URI ""
#define GW_USER ""
#define GW_PASSWORD ""

static GMainLoop *loop;

static gboolean
idle_run (gpointer data)
{
	GroupwiseConnection *gw;
	GroupwiseFolder folder;
	GroupwiseItem *item, new_item;
	GList *folders, *item_info, *l;
	char *id;
	
	gw = groupwise_connection_new (GW_URI, GW_USER, GW_PASSWORD);	

	if (!groupwise_connection_user_login (gw)) {
		g_warning (G_STRLOC ": Unable to login to server");
		goto cleanup;
	}

#if 0
	if (!groupwise_connection_folder_list (gw, &folders)) {
		g_warning (G_STRLOC ": Unable to fetch folder list");
		goto cleanup;
	}
#endif

#if 1
	new_item.drn = NULL;
	new_item.type = GROUPWISE_ITEM_TYPE_APPOINTMENT;
	new_item.to = GW_USER;
	new_item.subject = "Test";
	new_item.location = "Room202";
	new_item.message = NULL;
	new_item.start.year = 2003;
	new_item.start.month = 9;
	new_item.start.day = 4;
	new_item.start.hour = 6;
	new_item.start.minute = 0;
	new_item.start.am = FALSE;
	new_item.duration.type = GROUPWISE_DURATION_TYPE_HOURS;
	new_item.duration.length = 1;
	
	if (!groupwise_connection_compose (gw, new_item.type, &id)) {
		g_warning (G_STRLOC ": Unable to compose");
		goto cleanup;
	}
#endif

#if 1
	if (!groupwise_connection_compose_send (gw, id, &new_item, 4)) {
		g_warning (G_STRLOC ": Unable to send composition");
		goto cleanup;
	}
#endif

#if 0
	folder.id = 4;
	if (!groupwise_connection_folder_open (gw, &folder, &item_info)) {
		g_warning (G_STRLOC ": Unable to open folder");
		goto cleanup;
	}

	for (l = item_info; l; l = l->next) {
		GroupwiseItemInfo *info = l->data;
		
		if (!groupwise_connection_item_read (gw, info, &item)) {
			g_warning (G_STRLOC ": Unable to read item %d", info->id);
			continue;
		}
	}
#endif	

#if 0
	if (!groupwise_connection_timezone_list (gw)) {
		g_warning (G_STRLOC ": Unable to fetch timezone list");
		goto cleanup;
	}
#endif

	if (!groupwise_connection_user_logout (gw)) {
		g_warning (G_STRLOC ": Unable to logout of server");
		goto cleanup;
	}

	g_message ("Done testing");
 cleanup:	
	g_object_unref (gw);

	g_main_loop_quit (loop);
	
	return FALSE;
}

int
main (int argc, char *argv[])
{
	gnome_program_init ("gw-client", VERSION, LIBGNOME_MODULE, argc, argv, NULL);

	loop = g_main_loop_new (NULL, TRUE);
	g_idle_add (idle_run, NULL);
	g_main_loop_run (loop);

	return 0;
}
