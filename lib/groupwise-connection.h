/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GROUPWISE_CONNECTION_H
#define GROUPWISE_CONNECTION_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS



#define GROUPWISE_TYPE_CONNECTION            (groupwise_connection_get_type ())
#define GROUPWISE_CONNECTION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GROUPWISE_TYPE_CONNECTION, GroupwiseConnection))
#define GROUPWISE_CONNECTION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GROUPWISE_TYPE_CONNECTION,		\
				     GroupwiseConnectionClass))
#define GROUPWISE_IS_CONNECTION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GROUPWISE_TYPE_CONNECTION))
#define GROUPWISE_IS_CONNECTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GROUPWISE_TYPE_CONNECTION))

/* Open status values */
typedef struct _GroupwiseConnection GroupwiseConnection;
typedef struct _GroupwiseConnectionClass GroupwiseConnectionClass;
typedef struct _GroupwiseConnectionPrivate GroupwiseConnectionPrivate;

typedef enum {
	GROUPWISE_FOLDER_TYPE_TRASH,
	GROUPWISE_FOLDER_TYPE_UNIVERSAL,
	GROUPWISE_FOLDER_TYPE_CABINET,
	GROUPWISE_FOLDER_TYPE_USER,
	GROUPWISE_FOLDER_TYPE_UNOPENED,
	GROUPWISE_FOLDER_TYPE_SENTITEMS,
	GROUPWISE_FOLDER_TYPE_CHECKLIST,
	GROUPWISE_FOLDER_TYPE_SHARED,
	GROUPWISE_FOLDER_TYPE_QUERY,
	GROUPWISE_FOLDER_TYPE_CALENDAR,
	GROUPWISE_FOLDER_TYPE_OTHER
} GroupwiseFolderType;

typedef enum {
	GROUPWISE_ITEM_TYPE_MAIL,
	GROUPWISE_ITEM_TYPE_APPOINTMENT,
	GROUPWISE_ITEM_TYPE_NOTE,
	GROUPWISE_ITEM_TYPE_TASK,
	GROUPWISE_ITEM_TYPE_PHONE,
	GROUPWISE_ITEM_TYPE_OTHER
} GroupwiseItemType;

typedef enum {
	GROUPWISE_DURATION_TYPE_NONE,
	GROUPWISE_DURATION_TYPE_MINUTES,
	GROUPWISE_DURATION_TYPE_HOURS,
	GROUPWISE_DURATION_TYPE_DAYS,
} GroupwiseDurationType;

typedef struct {
	char *name;
	GroupwiseFolderType type;

	int id;
	
	int level;
	
	gboolean subfolders;
} GroupwiseFolder;

typedef struct {
	int id;

	char *drn;
} GroupwiseItemInfo;

typedef struct {
	short year;
	short month;
	short day;
	
	short hour;
	short minute;

	gboolean am;
} GroupwiseItemDate;

typedef struct {
	GroupwiseDurationType type;

	short length;
} GroupwiseItemDuration;

typedef struct {
	char *drn;
	GroupwiseItemType type;

	char *to;

	char *subject;
	char *location;
	char *message;
	
	GroupwiseItemDate start;
	GroupwiseItemDuration duration;
} GroupwiseItem;

struct _GroupwiseConnection {
	GObject object;

	GroupwiseConnectionPrivate *priv;
};

struct _GroupwiseConnectionClass {
	GObjectClass parent_class;
};

GType                groupwise_connection_get_type       (void);
GroupwiseConnection *groupwise_connection_new            (const char           *uri,
							  const char           *user,
							  const char           *password);
gboolean             groupwise_connection_user_login     (GroupwiseConnection  *gw);
gboolean             groupwise_connection_user_logout    (GroupwiseConnection  *gw);
gboolean             groupwise_connection_folder_list    (GroupwiseConnection  *gw,
							  GList               **folders);
gboolean             groupwise_connection_folder_open    (GroupwiseConnection  *gw,
							  int                   folder_id,
							  GList               **item_info);
gboolean             groupwise_connection_compose        (GroupwiseConnection  *gw,
							  GroupwiseItemType type,
							  char                **compose_id);
gboolean             groupwise_connection_compose_send   (GroupwiseConnection  *gw,
							  char                *compose_id,
							  GroupwiseItem *item, 
							  int folder_id);
gboolean             groupwise_connection_item_delete    (GroupwiseConnection  *gw,
							  const char           *drn);
gboolean             groupwise_connection_item_read      (GroupwiseConnection  *gw,
							  GroupwiseItemInfo    *info,
							  GroupwiseItem       **item);
gboolean             groupwise_connection_timezone_list  (GroupwiseConnection  *gw);
void                 groupwise_connection_folder_free    (GroupwiseFolder      *folder);
void                 groupwise_connection_item_info_free (GroupwiseItemInfo    *info);
void                 groupwise_connection_item_free      (GroupwiseItem        *item);

G_END_DECLS

#endif
