/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>

#include "groupwise-connection-cache.h"

static GHashTable *cache = NULL;

static void
connection_destroyed (gpointer data, GObject *where_object_was)
{
	g_hash_table_remove (cache, data);
}

static void
groupwise_connection_cache_init (void)
{
	if (cache)
		return;
	
	cache = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
}

void
groupwise_connection_cache_add (const char *uri, GroupwiseConnection *gw)
{
	char *uri_copy;
	
	groupwise_connection_cache_init ();

	uri_copy = g_strdup (uri);
	g_object_weak_ref (G_OBJECT (gw), connection_destroyed, uri_copy);
	
	g_hash_table_insert (cache, uri_copy, gw);

	g_message ("Adding %s", uri_copy);
}

GroupwiseConnection *
groupwise_connection_cache_find (const char *uri)
{
	groupwise_connection_cache_init ();

	g_message ("Finding %s", uri);
	
	return g_hash_table_lookup (cache, uri);
}

