/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <time.h>

#include "groupwise-soup-message.h"

extern int gw_debug_level;

#if GW_DEBUG
static void
print_header (gpointer name, gpointer value, gpointer data)
{
	printf ("%s: %s\n", (char *)name, (char *)value);
}

static void
gw_debug_handler (SoupMessage *msg, gpointer user_data)
{
	printf ("%d %s\nGw-Debug: %p @ %lu\n",
		msg->errorcode, msg->errorphrase,
		msg, time (0));
	if (gw_debug_level > 1) {
		soup_message_foreach_header (msg->response_headers,
					     print_header, NULL);
	}
	if (gw_debug_level > 2 && msg->response.length &&
	    GW_HTTP_STATUS_IS_SUCCESSFUL (msg->errorcode)) {
		const char *content_type =
			soup_message_get_header (msg->response_headers,
						 "Content-Type");
		if (!content_type || gw_debug_level > 4 ||
		    g_ascii_strcasecmp (content_type, "text/html")) {
			fputc ('\n', stdout);
			fwrite (msg->response.body, 1, msg->response.length, stdout);
			fputc ('\n', stdout);
		}
	}
	printf ("\n");
}

static void
gw_debug_setup (SoupMessage *msg)
{
	const SoupUri *uri;

	if (!gw_debug_level)
		return;

	uri = soup_context_get_uri (msg->context);
	printf ("%s %s%s%s HTTP/1.1\nGw-Debug: %p @ %lu\n",
		msg->method, uri->path,
		uri->querystring ? "?" : "",
		uri->querystring ? uri->querystring : "",
		msg, (unsigned long)time (0));
	if (gw_debug_level > 1) {
		print_header ("Host", uri->host, NULL);
		soup_message_foreach_header (msg->request_headers,
					     print_header, NULL);
	}
	if (gw_debug_level > 2 && msg->request.length) {
		fputc ('\n', stdout);
		fwrite (msg->request.body, 1, msg->request.length, stdout);
	}
	printf ("\n");

	soup_message_add_handler (msg, SOUP_HANDLER_POST_BODY,
				  gw_debug_handler, NULL);
}
#endif

static void
setup_message (SoupMessage *msg)
{
	soup_message_add_header (msg->request_headers, "User-Agent",
				 "Evolution/" EVOLUTION_VERSION);
}

SoupMessage *
groupwise_soup_message_new (SoupUri *uri, const char *method)
{
	SoupContext *sctx;
	SoupMessage *msg;

 	sctx = soup_context_from_uri (uri);
	msg = soup_message_new (sctx, method);
	setup_message (msg);
	soup_context_unref (sctx);

	return msg;
}

SoupMessage *
groupwise_soup_message_new_full (SoupUri *uri, const char *method,
				 SoupOwnership  owner, const char *body, gulong length)
{
	SoupContext *sctx;
	SoupMessage *msg;

	sctx = soup_context_from_uri (uri);
	msg = soup_message_new_full (sctx, method, owner, (char *)body, length);
	setup_message (msg);
	soup_context_unref (sctx);

	return msg;
}

SoupErrorClass 
groupwise_soup_message_send (SoupMessage *msg)
{
#ifdef GW_DEBUG
	gw_debug_setup (msg);
#endif

	return soup_message_send (msg);
}
