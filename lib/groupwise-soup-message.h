/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libsoup/soup-message.h>
#include <libsoup/soup-uri.h>

#define GW_HTTP_STATUS_IS_SUCCESSFUL(x)        ((x) >= 200 && (x) < 300)

SoupMessage   *groupwise_soup_message_new      (SoupUri       *uri,
						const char    *method);
SoupMessage   *groupwise_soup_message_new_full (SoupUri       *uri,
						const char    *method,
						SoupOwnership  owner,
						const char    *body,
						gulong         length);
SoupErrorClass groupwise_soup_message_send     (SoupMessage   *msg);

