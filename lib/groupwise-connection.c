/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>

#include "groupwise-soup-message.h"
#include "groupwise-connection.h"

struct _GroupwiseConnectionPrivate {
	char *uri;
	char *user;
	char *password;
	
	char *context;
	
	gboolean logged_in;
};

enum props {
	PROP_0,
	PROP_URI,
	PROP_USER,
	PROP_PASSWORD
};

static GObjectClass *parent_class;

#define NEW_URI(uri) {uri = soup_uri_new (priv->uri); \
                     if (uri && uri->path) {g_free (uri->path); uri->path = NULL;} \
                     if (uri && uri->querystring) {g_free (uri->querystring); uri->querystring = NULL;}}

#ifdef GW_DEBUG
char *gw_debug;
int gw_debug_level;
#endif

static void
groupwise_connection_init (GroupwiseConnection *gw)
{
	GroupwiseConnectionPrivate *priv;

	priv = g_new0 (GroupwiseConnectionPrivate, 1);
	gw->priv = priv;

	priv->logged_in = FALSE;
}

static void
groupwise_connection_set_property (GObject *object, guint property_id, 
				   const GValue *value, GParamSpec *pspec)
{
	GroupwiseConnection *gw;
	GroupwiseConnectionPrivate *priv;
	
	gw = GROUPWISE_CONNECTION (object);
	priv = gw->priv;
	
	switch (property_id) {
	case PROP_URI:
		priv->uri = g_value_dup_string (value);
		break;
	case PROP_USER:
		priv->user = g_value_dup_string (value);
		break;
	case PROP_PASSWORD:
		priv->password = g_value_dup_string (value);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
groupwise_connection_get_property (GObject *object, guint property_id, 
				   GValue *value, GParamSpec *pspec)
{
	GroupwiseConnection *gw;
	GroupwiseConnectionPrivate *priv;
	
	gw = GROUPWISE_CONNECTION (object);
	priv = gw->priv;

	switch (property_id) {
	case PROP_URI:
		g_value_set_string (value, priv->uri);
		break;
	case PROP_USER:
		g_value_set_string (value, priv->user);
		break;
	case PROP_PASSWORD:
		g_value_set_string (value, priv->password);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
groupwise_connection_finalize (GObject *object) 
{
	GroupwiseConnection *gw;
	GroupwiseConnectionPrivate *priv;
	
	gw = GROUPWISE_CONNECTION (object);
	priv = gw->priv;
	
	g_free (priv->uri);
	g_free (priv->user);
	g_free (priv->password);
	
	g_free (priv);
}

static void
groupwise_connection_class_init (GroupwiseConnectionClass *klass)
{
	GObjectClass *object_class;
	GParamSpec *param;
	
	parent_class = (GObjectClass *) g_type_class_peek_parent (klass);

	object_class = (GObjectClass *) klass;

	object_class->get_property = groupwise_connection_get_property;
	object_class->set_property = groupwise_connection_set_property;
	object_class->finalize = groupwise_connection_finalize;

	param = g_param_spec_string ("uri", NULL, NULL, "",
				     G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
	g_object_class_install_property (object_class, PROP_URI, param);

	param = g_param_spec_string ("user", NULL, NULL, "",
				     G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
	g_object_class_install_property (object_class, PROP_USER, param);

	param = g_param_spec_string ("password", NULL, NULL, "",
				     G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
	g_object_class_install_property (object_class, PROP_PASSWORD, param);

	
#ifdef GW_DEBUG
	gw_debug = getenv ("GW_DEBUG");
	if (gw_debug)
		gw_debug_level = atoi (gw_debug);
#endif
}

GType
groupwise_connection_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static GTypeInfo info = {
                        sizeof (GroupwiseConnectionClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) groupwise_connection_class_init,
                        NULL, NULL,
                        sizeof (GroupwiseConnection),
                        0,
                        (GInstanceInitFunc) groupwise_connection_init,
                };
		type = g_type_register_static (G_TYPE_OBJECT, "GroupwiseConnection", &info, 0);
	}

	return type;
}

GroupwiseConnection *
groupwise_connection_new (const char *uri, const char *user, const char *password)
{
	GroupwiseConnection *gw;
	
	gw = g_object_new (GROUPWISE_TYPE_CONNECTION, "uri", uri, 
			    "user", user, "password", password, NULL);

	return gw;
}

static char *
xml_get_content (xmlNodePtr node)
{
	char *buf, *new_val;

	buf = xmlNodeGetContent (node);
        new_val = g_strdup (buf);
	xmlFree (buf);

	return new_val;
}

static int
xml_get_int (xmlNodePtr node)
{
	int val;
	char *buf;
	
	buf = xmlNodeGetContent (node);
	val = atoi (buf);
	xmlFree (buf);

	return val;
}


static time_t
xml_get_timet (xmlNodePtr node)
{
	time_t val;
	char *buf;

	buf = xmlNodeGetContent (node);
	val = strtoull (buf, NULL, 10) / 1000;
	xmlFree (buf);

	return val;
}

static const char *
item_type_to_string (GroupwiseItemType item_type)
{
	switch (item_type) {
	case GROUPWISE_ITEM_TYPE_MAIL:
		return "Mail";		
	case GROUPWISE_ITEM_TYPE_APPOINTMENT:
		return "Appointment";
	case GROUPWISE_ITEM_TYPE_NOTE:
		return "Note";
	case GROUPWISE_ITEM_TYPE_TASK:
		return "Task";
	case GROUPWISE_ITEM_TYPE_PHONE:
		return "Phone";
	default:
		return "Mail";
	}

	return "Mail";
}


static char *
query_encode (const char *part, gboolean escape_unsafe,
	      const char *escape_extra)
{
	char *work, *p;

	/* worst case scenario = 3 times the initial */
	p = work = g_malloc (3 * strlen (part) + 1);

	while (*part) {
		if (((guchar) *part >= 127) || ((guchar) *part <= ' ') ||
		    (escape_unsafe && strchr ("\"%#<>{}|\\^~[]`", *part)) ||
		    (escape_extra && strchr (escape_extra, *part))) {
			sprintf (p, "%%%.02hX", (guchar) *part++);
			p += 3;
		} else
			*p++ = *part++;
	}
	*p = '\0';
	
	return work;
}

gboolean
groupwise_connection_user_login (GroupwiseConnection *gw)
{
	GroupwiseConnectionPrivate *priv;
	SoupMessage *msg;
	SoupUri *uri;
	gboolean retval;
	char *body;
	
	g_return_val_if_fail (gw != NULL, FALSE);
	g_return_val_if_fail (GROUPWISE_IS_CONNECTION (gw), FALSE);

	priv = gw->priv;
	
	if (priv->logged_in)
		return TRUE;
	
	NEW_URI (uri);
	if (!uri)
		return FALSE;
	
	uri->path = g_strdup ("/servlet/webacc");
	body = g_strdup_printf ("action=User.Login&User.id=%s&User.password=%s"
				"&User.interface=GWServer&merge=user_info&error=server_error",
				priv->user, priv->password);
	
	msg = groupwise_soup_message_new_full (uri, SOUP_METHOD_POST, SOUP_BUFFER_SYSTEM_OWNED,
					       body, strlen (body));
	
	soup_message_add_header (msg->request_headers, "Content-type",
				 "application/x-www-form-urlencoded");

	groupwise_soup_message_send (msg);

	retval = GW_HTTP_STATUS_IS_SUCCESSFUL (msg->errorcode);
	if (retval) {
		xmlNodePtr node;
		xmlDocPtr doc;

		if (!(doc = xmlParseDoc (msg->response.body)))
			return FALSE;
		
		node = doc->children;
		if (!strcmp (node->name, "server_error")) {
			xmlFreeDoc (doc);
			return FALSE;
		} else if (strcmp (node->name, "user_info")) {
			xmlFreeDoc (doc);
			return FALSE;
		}

		for (node = node->children; node; node = node->next) {
			if (!strcmp (node->name, "context"))
				priv->context = xml_get_content (node);

			/* FIXME Check provider version? */
		}

		if (priv->context)
			priv->logged_in = TRUE;
		else
			retval = FALSE;
	}
	
	soup_uri_free (uri);
	
	return retval;
}

gboolean
groupwise_connection_user_logout (GroupwiseConnection *gw)
{
	GroupwiseConnectionPrivate *priv;
	SoupMessage *msg;
	SoupUri *uri;
	gboolean retval;
	char *body;
	
	g_return_val_if_fail (gw != NULL, FALSE);
	g_return_val_if_fail (GROUPWISE_IS_CONNECTION (gw), FALSE);

	priv = gw->priv;
	
	if (!priv->logged_in)
		return TRUE;

	NEW_URI (uri);
	if (!uri)
		return FALSE;
	
	uri->path = g_strdup ("/servlet/webacc");
	body = g_strdup_printf ("action=User.Logout&User.context=%s"
				"&User.interface=GWServer&merge=user_info&error=server_error", 
				priv->context);

	msg = groupwise_soup_message_new_full (uri, SOUP_METHOD_POST, SOUP_BUFFER_SYSTEM_OWNED,
					       body, strlen (body));
	
	soup_message_add_header (msg->request_headers, "Content-type",
				 "application/x-www-form-urlencoded");
	
	groupwise_soup_message_send (msg);

	retval = GW_HTTP_STATUS_IS_SUCCESSFUL (msg->errorcode);

	if (retval) {
		g_free (priv->context);
		priv->context = NULL;
		priv->logged_in = FALSE;
	}
	
	soup_message_free (msg);
	soup_uri_free (uri);

	return retval;
}

gboolean 
groupwise_connection_folder_list (GroupwiseConnection *gw, GList **folders)
{
	GroupwiseConnectionPrivate *priv;
	SoupMessage *msg;
	SoupUri *uri;
	gboolean retval;
	char *body;
	
	g_return_val_if_fail (gw != NULL, FALSE);
	g_return_val_if_fail (GROUPWISE_IS_CONNECTION (gw), FALSE);
	g_return_val_if_fail (folders, FALSE);

	priv = gw->priv;
	
	if (!priv->logged_in)
		return FALSE;
	
	NEW_URI (uri);
	if (!uri)
		return FALSE;
	
	uri->path = g_strdup ("/servlet/webacc");
	body = g_strdup_printf ("action=Folder.List&User.context=%s"
				"&merge=folder_list&error=server_error",
				priv->context);

	msg = groupwise_soup_message_new_full (uri, SOUP_METHOD_POST, SOUP_BUFFER_SYSTEM_OWNED,
					       body, strlen (body));
	
	soup_message_add_header (msg->request_headers, "Content-type",
				 "application/x-www-form-urlencoded");
	
	groupwise_soup_message_send (msg);

	retval = GW_HTTP_STATUS_IS_SUCCESSFUL (msg->errorcode);
	if (retval) {
		GroupwiseFolder *folder;
		xmlNodePtr node, subnode;
		xmlDocPtr doc;

		if (!(doc = xmlParseDoc (msg->response.body)))
			return FALSE;
		
		node = doc->children;
		if (!strcmp (node->name, "server_error")) {
			xmlFreeDoc (doc);
			return FALSE;
		} else if (strcmp (node->name, "fldLst")) {
			xmlFreeDoc (doc);
			return FALSE;
		}

		*folders = NULL;
		for (node = node->children; node; node = node->next) {
			if (!strcmp (node->name, "fld")) {
				folder = g_new0 (GroupwiseFolder, 1);

				for (subnode = node->children; subnode; subnode = subnode->next) {
					if (!strcmp (subnode->name, "id")) {
						folder->id = xml_get_int (subnode);
					} else if (!strcmp (subnode->name, "nm")) {
						folder->name = xml_get_content (subnode);
					} else if (!strcmp (subnode->name, "tp")) {
						char *type = xml_get_content (subnode);
											
						/* CHECKLIST and TASKLIST are both CHECKLIST because 
						   TASKLIST was used in older versions */
						if (!strcmp (type, "Folder.TRASH"))
							folder->type = GROUPWISE_FOLDER_TYPE_TRASH;
						else if (!strcmp (type, "Folder.UNIVERSAL"))
							folder->type = GROUPWISE_FOLDER_TYPE_UNIVERSAL;
						else if (!strcmp (type, "Folder.CABINET"))
							folder->type = GROUPWISE_FOLDER_TYPE_CABINET;
						else if (!strcmp (type, "Folder.USER"))
							folder->type = GROUPWISE_FOLDER_TYPE_USER;
						else if (!strcmp (type, "Folder.UNOPENED"))
							folder->type = GROUPWISE_FOLDER_TYPE_UNOPENED;
						else if (!strcmp (type, "Folder.SENTITEMS"))
							folder->type = GROUPWISE_FOLDER_TYPE_SENTITEMS;
						else if (!strcmp (type, "Folder.TASKLIST"))
							folder->type = GROUPWISE_FOLDER_TYPE_CHECKLIST;
						else if (!strcmp (type, "Folder.CHECKLIST"))
							folder->type = GROUPWISE_FOLDER_TYPE_CHECKLIST;
						else if (!strcmp (type, "Folder.SHARED"))
							folder->type = GROUPWISE_FOLDER_TYPE_SHARED;
						else if (!strcmp (type, "Folder.QUERY"))
							folder->type = GROUPWISE_FOLDER_TYPE_QUERY;
						else if (!strcmp (type, "Folder.CALENDAR"))
							folder->type = GROUPWISE_FOLDER_TYPE_CALENDAR;
						else
							folder->type = GROUPWISE_FOLDER_TYPE_OTHER;
						g_free (type);
					}  else if (!strcmp (subnode->name, "Sb")) {
						char *sub = xml_get_content (subnode);

						folder->subfolders = (!strcmp (sub, "true"));

						g_free (sub);
					} else if (!strcmp (subnode->name, "lv")) {
						folder->level = xml_get_int (subnode);
					}	

				}
				*folders = g_list_append (*folders, folder);
			}
		}
	}
	
	soup_uri_free (uri);
	
	return retval;
}

gboolean
groupwise_connection_folder_open (GroupwiseConnection *gw, int folder_id, GList **item_info)
{
	GroupwiseConnectionPrivate *priv;
	SoupMessage *msg;
	SoupUri *uri;
	gboolean retval;
	char *body;
	
	g_return_val_if_fail (gw != NULL, FALSE);
	g_return_val_if_fail (GROUPWISE_IS_CONNECTION (gw), FALSE);
	g_return_val_if_fail (item_info, FALSE);

	priv = gw->priv;
	
	if (!priv->logged_in)
		return FALSE;
	
	NEW_URI (uri);
	if (!uri)
		return FALSE;
	
	uri->path = g_strdup ("/servlet/webacc");
	body = g_strdup_printf ("action=Folder.Open&User.context=%s&Folder.id=%d"
				"&merge=item_drn_list&error=server_error",
				priv->context, folder_id);

	msg = groupwise_soup_message_new_full (uri, SOUP_METHOD_POST, SOUP_BUFFER_SYSTEM_OWNED,
					       body, strlen (body));
	
	soup_message_add_header (msg->request_headers, "Content-type",
				 "application/x-www-form-urlencoded");
	
	groupwise_soup_message_send (msg);

	retval = GW_HTTP_STATUS_IS_SUCCESSFUL (msg->errorcode);
	if (retval) {
		GroupwiseItemInfo *info;
		xmlNodePtr node, subnode;
		xmlDocPtr doc;

		if (!(doc = xmlParseDoc (msg->response.body)))
			return FALSE;
		
		node = doc->children;
		if (!strcmp (node->name, "server_error")) {
			xmlFreeDoc (doc);
			return FALSE;
		} else if (strcmp (node->name, "item_drn_list")) {
			xmlFreeDoc (doc);
			return FALSE;
		}

		*item_info = NULL;
		for (node = node->children; node; node = node->next) {
			if (!strcmp (node->name, "gwitem")) {
				info = g_new0 (GroupwiseItemInfo, 1);

				for (subnode = node->children; subnode; subnode = subnode->next) {
					if (!strcmp (subnode->name, "id")) {
						info->id = xml_get_int (subnode);
					} else if (!strcmp (subnode->name, "drn")) {
						info->drn = xml_get_content (subnode);
					}
				}
				*item_info = g_list_append (*item_info, info);
			}
		}
	}
	
	soup_uri_free (uri);
	
	return retval;
}

gboolean
groupwise_connection_compose (GroupwiseConnection  *gw, GroupwiseItemType type, char **compose_id)
{
	GroupwiseConnectionPrivate *priv;
	SoupMessage *msg;
	SoupUri *uri;
	gboolean retval;
	char *body;
	
	g_return_val_if_fail (gw != NULL, FALSE);
	g_return_val_if_fail (GROUPWISE_IS_CONNECTION (gw), FALSE);

	priv = gw->priv;
	
	if (!priv->logged_in)
		return FALSE;
	
	NEW_URI (uri);
	if (!uri)
		return FALSE;
	
	uri->path = g_strdup ("/servlet/webacc");
	body = g_strdup_printf ("action=Compose.Action&User.context=%s"
				"&Item.type=%s&merge=compose",
				priv->context, item_type_to_string (type));

	msg = groupwise_soup_message_new_full (uri, SOUP_METHOD_POST, SOUP_BUFFER_SYSTEM_OWNED,
					       body, strlen (body));
	
	soup_message_add_header (msg->request_headers, "Content-type",
				 "application/x-www-form-urlencoded");
	
	groupwise_soup_message_send (msg);

	retval = GW_HTTP_STATUS_IS_SUCCESSFUL (msg->errorcode);
	if (retval) {
		xmlNodePtr node;
		xmlDocPtr doc;

		if (!(doc = xmlParseDoc (msg->response.body)))
			return FALSE;
		
		node = doc->children;
		if (!strcmp (node->name, "server_error")) {
			xmlFreeDoc (doc);
			return FALSE;
		} else if (strcmp (node->name, "compose")) {
			xmlFreeDoc (doc);
			return FALSE;
		}
		
		*compose_id = NULL;
		for (node = node->children; node; node = node->next) {
			if (!strcmp (node->name, "composeID"))
				*compose_id = xml_get_content (node);
		}

		if (!*compose_id)
			retval = FALSE;
	}
	
	return retval;
}

gboolean
groupwise_connection_compose_send (GroupwiseConnection *gw, char *compose_id, 
				   GroupwiseItem *item, int folder_id)
{
	GroupwiseConnectionPrivate *priv;
	SoupMessage *msg;
	SoupUri *uri;
	gboolean retval;
	char *body, *enc;

	g_return_val_if_fail (gw != NULL, FALSE);
	g_return_val_if_fail (GROUPWISE_IS_CONNECTION (gw), FALSE);
	g_return_val_if_fail (compose_id != NULL, FALSE);

	priv = gw->priv;
	
	if (!priv->logged_in)
		return FALSE;
	
	NEW_URI (uri);
	if (!uri)
		return FALSE;
	
	uri->path = g_strdup ("/servlet/webacc");
#if 1

	body = g_strdup_printf ("action=Compose.Action&Compose.Send=Send&User.context=%s"
				"&Item.type=%s&Item.to=%s"
				"&Item.location=%s&Item.subject=%s&Item.message=%s"
				"&Calendar.queryYear=%d&Calendar.queryMonth=%d&Calendar.queryDay=%d"
				"&Calendar.queryAM=%s&Calendar.queryTime=%d:%d"
				"&Calendar.queryDurationType=%d&Calendar.queryDuration=%d"
				"&Compose.id=%s"
				"&merge=item&error=cmp_error",
				priv->context, 
				item_type_to_string (item->type),
				item->to ? item->to : "",
				item->location ? item->location : "",
				item->subject ? item->subject : "",
				item->message ? item->message : "",
				item->start.year, item->start.month, item->start.day,
				item->start.am ? "1" : "0", item->start.hour, item->start.minute,
				item->duration.type, item->duration.length,
				compose_id);
	
#else
	body = g_strdup_printf ("action=Compose.Action&Compose.Send=Send&User.context=%s"
				"&Item.type=Appointment&Compose.id=%s&Item.to=test1&Item.location="
				"&Calendar.queryYear=2003&Calendar.queryMonth=9&Calendar.queryDay=2"
				"&Calendar.queryAM=0&Calendar.queryTime=5:00"
				"&Calendar.queryDurationType=2&Calendar.queryDuration=1"
				"&Item.subject=Ick&Item.message=yahaha"
				"&merge=compose&error=cmp_error",
				priv->context, compose_id);

#endif
	
	enc = query_encode (body, TRUE, ":");
	g_free (body);
	
	msg = groupwise_soup_message_new_full (uri, SOUP_METHOD_POST, SOUP_BUFFER_SYSTEM_OWNED,
					       enc, strlen (enc));
	
	soup_message_add_header (msg->request_headers, "Content-type",
				 "application/x-www-form-urlencoded");	
	
	groupwise_soup_message_send (msg);

	retval = GW_HTTP_STATUS_IS_SUCCESSFUL (msg->errorcode);

	if (retval) {
		xmlNodePtr node;
		xmlDocPtr doc;
		
		if (!(doc = xmlParseDoc (msg->response.body)))
			return FALSE;
		
		node = doc->children;
		if (!strcmp (node->name, "server_error")) {
			xmlFreeDoc (doc);
			return FALSE;
		} else if (strcmp (node->name, "gwitem")) {
			xmlFreeDoc (doc);
			return FALSE;
		}

		for (node = node->children; node; node = node->next) {
			if (!strcmp (node->name, "drn")) {
				g_free (item->drn);
				item->drn = xml_get_content (node);
			}
		}
	}
	
	soup_uri_free (uri);
	
	return retval;
}

gboolean
groupwise_connection_item_delete (GroupwiseConnection *gw, const char *drn) 
{
	GroupwiseConnectionPrivate *priv;
	SoupMessage *msg;
	SoupUri *uri;
	gboolean retval;
	
	g_return_val_if_fail (gw != NULL, FALSE);
	g_return_val_if_fail (GROUPWISE_IS_CONNECTION (gw), FALSE);
	g_return_val_if_fail (drn != NULL, FALSE);

	priv = gw->priv;
	
	if (!priv->logged_in)
		return FALSE;
	
	NEW_URI (uri);
	if (!uri)
		return FALSE;
	
	uri->path = g_strdup ("/servlet/webacc");
	uri->querystring = g_strdup_printf ("action=Item.Delete&User.context=%s&Item.drn=%s"
					    "&merge=item&error=server_error",
					    priv->context, drn);
	
	msg = groupwise_soup_message_new (uri, SOUP_METHOD_GET);
	
	groupwise_soup_message_send (msg);

	retval = GW_HTTP_STATUS_IS_SUCCESSFUL (msg->errorcode);
	
	return retval;
}

gboolean
groupwise_connection_item_read (GroupwiseConnection *gw, GroupwiseItemInfo *info, GroupwiseItem **item)
{
	GroupwiseConnectionPrivate *priv;
	SoupMessage *msg;
	SoupUri *uri;
	gboolean retval;
	struct tm t;
	
	g_return_val_if_fail (gw != NULL, FALSE);
	g_return_val_if_fail (GROUPWISE_IS_CONNECTION (gw), FALSE);
	g_return_val_if_fail (info != NULL, FALSE);

	priv = gw->priv;
	
	if (!priv->logged_in)
		return FALSE;
	
	NEW_URI (uri);
	if (!uri)
		return FALSE;
	
	uri->path = g_strdup ("/servlet/webacc");
	uri->querystring = g_strdup_printf ("action=Item.Read&User.context=%s&Item.drn=%s"
					    "&merge=item&error=server_error",
					    priv->context, info->drn);
	
	msg = groupwise_soup_message_new (uri, SOUP_METHOD_GET);
	
	groupwise_soup_message_send (msg);

	retval = GW_HTTP_STATUS_IS_SUCCESSFUL (msg->errorcode);
	if (retval) {
		xmlNodePtr node;
		xmlDocPtr doc;
		time_t start = 0, end = 0;
		
		if (!(doc = xmlParseDoc (msg->response.body)))
			return FALSE;
		
		node = doc->children;
		if (!strcmp (node->name, "server_error")) {
			xmlFreeDoc (doc);
			return FALSE;
		} else if (strcmp (node->name, "gwitem")) {
			xmlFreeDoc (doc);
			return FALSE;
		}

		*item = g_new0 (GroupwiseItem, 1);
		for (node = node->children; node; node = node->next) {
			if (!strcmp (node->name, "drn")) {
				(*item)->drn = xml_get_content (node);
			} else if (!strcmp (node->name, "tp")) {
				char *type = xml_get_content (node);
												
				if (!strcmp (type, "Mail"))
					(*item)->type = GROUPWISE_ITEM_TYPE_MAIL;
				else if (!strcmp (type, "Appointment"))
					(*item)->type = GROUPWISE_ITEM_TYPE_APPOINTMENT;
				else if (!strcmp (type, "Note"))
					(*item)->type = GROUPWISE_ITEM_TYPE_NOTE;
				else if (!strcmp (type, "Task"))
					(*item)->type = GROUPWISE_ITEM_TYPE_TASK;
				else if (!strcmp (type, "Phone"))
					(*item)->type = GROUPWISE_ITEM_TYPE_PHONE;
				else
					(*item)->type = GROUPWISE_ITEM_TYPE_OTHER;

				g_free (type);
			} else if (!strcmp (node->name, "subj")) {
				(*item)->subject = xml_get_content (node);
			} else if (!strcmp (node->name, "loc")) {
				(*item)->location = xml_get_content (node);
			} else if (!strcmp (node->name, "msg")) {
				(*item)->message = xml_get_content (node);
			} else if (!strcmp (node->name, "dSta")) {
				start = xml_get_timet (node);
			} else if (!strcmp (node->name, "dEnd")) {
				end = xml_get_timet (node);
			}
		}

		if (!start || !end) {
			groupwise_connection_item_free (*item);
			return FALSE;
		}

		t = *(gmtime(&start));
		(*item)->start.year = t.tm_year + 1900;
		(*item)->start.month = t.tm_mon + 1;
		(*item)->start.day = t.tm_mday;
		(*item)->start.hour = t.tm_hour % 12;
		if ((*item)->start.hour == 0)
			(*item)->start.hour = 12;
		(*item)->start.minute = t.tm_min;
		(*item)->start.am = t.tm_hour > 11 ? FALSE : TRUE;

		if ((end - start) % (60 * 60 * 24) == 0) {
			/* Evenly divisible by whole days */
			(*item)->duration.type = GROUPWISE_DURATION_TYPE_DAYS;
			(*item)->duration.length = (end - start) / (60 * 60 * 24);
		} else if ((end - start) % (60 * 60) == 0) {
			/* Evenly divisible by whole hours */
			(*item)->duration.type = GROUPWISE_DURATION_TYPE_HOURS;
			(*item)->duration.length = (end - start) / (60 * 60);
		} else {
			(*item)->duration.type = GROUPWISE_DURATION_TYPE_MINUTES;
			(*item)->duration.length = (end - start) / (60);
		}
	}
	
	soup_uri_free (uri);
	
	return retval;	
}

gboolean
groupwise_connection_timezone_list (GroupwiseConnection *gw)
{
	GroupwiseConnectionPrivate *priv;
	SoupMessage *msg;
	SoupUri *uri;
	gboolean retval;
	
	g_return_val_if_fail (gw != NULL, FALSE);
	g_return_val_if_fail (GROUPWISE_IS_CONNECTION (gw), FALSE);

	priv = gw->priv;
	
	if (!priv->logged_in)
		return FALSE;
	
	NEW_URI (uri);
	if (!uri)
		return FALSE;
	
	uri->path = g_strdup ("/servlet/webacc");
	uri->querystring = g_strdup_printf ("action=TimeZone.List&User.context=%s"
					    "&User.interface=GWServer&merge=timezone&error=server_error",
					    priv->context);
	
	msg = groupwise_soup_message_new (uri, SOUP_METHOD_GET);
	
	groupwise_soup_message_send (msg);

	retval = GW_HTTP_STATUS_IS_SUCCESSFUL (msg->errorcode);
	if (retval) {
		xmlNodePtr node;
		xmlDocPtr doc;

		if (!(doc = xmlParseDoc (msg->response.body)))
			return FALSE;
		
		node = doc->children;
		if (!strcmp (node->name, "server_error")) {
			xmlFreeDoc (doc);
			return FALSE;
		} else if (strcmp (node->name, "timezone_list")) {
			xmlFreeDoc (doc);
			return FALSE;
		}

		for (node = node->children; node; node = node->next) {
		}
	}
	
	soup_uri_free (uri);
	
	return retval;
}

void
groupwise_connection_folder_free (GroupwiseFolder *folder)
{
	g_free (folder->name);
	g_free (folder);
}

void
groupwise_connection_item_info_free (GroupwiseItemInfo *info)
{
	g_free (info->drn);
	
	g_free (info);
}

void
groupwise_connection_item_free (GroupwiseItem *item)
{
	g_free (item->drn);
	g_free (item->to);
	g_free (item->subject);
	g_free (item->location);
	g_free (item->message);
	
	g_free (item);
}
