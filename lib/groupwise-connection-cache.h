/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Author : 
 *  JP Rosevear <jpr@ximian.com>
 *
 * Copyright 2003, Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of version 2 of the GNU General Public 
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GROUPWISE_CONNECTION_CACHE_H
#define GROUPWISE_CONNECTION_CACHE_H

#include <glib.h>
#include <glib-object.h>
#include "groupwise-connection.h"

G_BEGIN_DECLS

void groupwise_connection_cache_add (const char *uri, GroupwiseConnection *gw);
GroupwiseConnection *groupwise_connection_cache_find (const char *uri);

G_END_DECLS

#endif
